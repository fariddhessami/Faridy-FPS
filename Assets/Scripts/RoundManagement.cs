﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoundManagement : MonoBehaviour
{
    [SerializeField] private static float _roundSeconds = 15.0f;
    [SerializeField] private static float _roundFinishSeconds = 5.0f;
    private static Timer _timer; 
    
    
    public RoundState _roundState {  get; private set; }

    public bool AmIServer = false;
    
    //    singleTone stuff:
    public static RoundManagement _instance {  get; private set; }
    public static RoundManagement Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<RoundManagement>();
            }
            return _instance;
        }
    }
    
    
    void Awake()
    {
        _timer = new Timer();
        _roundState = RoundState.Finished;
    }

    public void StartTheRound()
    {
        _roundState = RoundState.OnGoing;
        _timer.StartCountDown(_roundSeconds);
    }
    
    void Update()
    {
        _timer.UpdateCountDown();

        if (AmIServer)
        {
            if (_timer.IsCountDownFinished)
            {
                GameManagement.Instance.ChangeRoundProcessByServer();
            }
        }
    }

    public String GetTimeDisplay()
    {
        return _timer.DisplayTime();
    }

    public float GetRemainingTimeFloat()
    {
        return _timer.CountDownValue;
    }

    
    public static RoundState IntToRoundState(int i)
    {
        //    this mapping method is for synchronizing round info
        //    through the network
        
        switch (i)
        {
            case 0: return RoundState.Finished;
            case 1: return RoundState.OnGoing;
            default: throw new 
                Exception("undefined RoundState int");
        }
    }
    
    public static int RoundStateToInt(RoundState rs)
    {
        //    this mapping method is for synchronizing round info
        //    through the network
        
        switch (rs)
        {
            case RoundState.Finished : return 0;
            case RoundState.OnGoing : return 1;
            default: throw new 
                Exception("undefined int");
        }
    }

    public void UpdateGameInfoByServerSynch(RoundState currentRoundState, float currentRoundTime)
    {
        _roundState = currentRoundState;
        _timer.UpdateTimerData(currentRoundTime);
    }

    public void SetRoundManagerToBeServer()
    {
        AmIServer = true;
    }

    public static float DurationByRoundState(RoundState nextRoundState)
    {
        switch (nextRoundState)
        {
            case RoundState.Finished: return _roundFinishSeconds;
            default: return _roundSeconds;
        }
    }
}

public enum RoundState
{
    Finished,
    OnGoing
}
