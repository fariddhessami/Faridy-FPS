﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingManagement : MonoBehaviour
{
    
    //    singleTone stuff:
    private static SettingManagement _instance;
    public static SettingManagement Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<SettingManagement>();
            }
            return _instance;
        }
    }
    
    [SerializeField] private String _myPlayerName; 
    
    
    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetMyPlayerName(String name)
    {
        this._myPlayerName = name;
    }
    
    public String GetMyPlayerName()
    {
        return this._myPlayerName;
    }
    
}
