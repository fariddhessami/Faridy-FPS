﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraDirector : MonoBehaviour
{

    [SerializeField] public Camera _myFPS_camera { get; set;}
    [SerializeField] public AudioListener _myFPS_al { get; set;}
    [SerializeField] public Camera _map_camera { get; set;}
    [SerializeField] public AudioListener _map_al { get; set;}
    
    [SerializeField] public Camera _my3rdP_camera { get; set;}
    [SerializeField] public AudioListener _my3rdP_al { get; set;}
    
    
    public GameObject My3RdPersonModel { get; set;}
    public SkinnedMeshRenderer My3RdPersonSkinedMesh { get; set;}

    
    [SerializeField] public WeaponHolder _my3rdP_WH { get; set;}
    
    [SerializeField] public WeaponHolder _myFPS_WH { get; set;}
    
    public CameraViewStatus _CameraViewStatus { get; set; }



    public Camera GetActiveCamera()
    {

        switch (_CameraViewStatus)
        {
            case CameraViewStatus.map_view : return _map_camera;
            case CameraViewStatus.player_view : return _myFPS_camera;
            case CameraViewStatus.thirdP_view : return _my3rdP_camera;
            default : return _map_camera;
        }
    }


    void Initialize()
    {
        
    }
    
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    
    internal void Switch_camera_to_myPlayer()
    {
        Disable_all_cams_als();
        
        _myFPS_camera.enabled = true;
        _myFPS_al.enabled = true;
        
        // _map_camera.enabled = false;
        // _map_al.enabled = false;


        My3RdPersonSkinedMesh.enabled = false;
        _CameraViewStatus = CameraViewStatus.player_view;
        
        _myFPS_WH.Turn_display_on();
    }
    internal void Switch_camera_to_map()
    {
        Disable_all_cams_als();
        
        // _myFPS_camera.enabled = false;
        // _myFPS_al.enabled = false;
        
        _map_camera.enabled = true;
        _map_al.enabled = true;
        
        My3RdPersonSkinedMesh.enabled = true;
        _CameraViewStatus = CameraViewStatus.map_view;
        
        _my3rdP_WH.Turn_display_on();
    }
    
    internal void Switch_camera_to_3rdPerson()
    {
        Disable_all_cams_als();

        _my3rdP_camera.enabled = true;
        _my3rdP_al.enabled = true;
        
        My3RdPersonSkinedMesh.enabled = true;
        _CameraViewStatus = CameraViewStatus.thirdP_view;
        
        _my3rdP_WH.Turn_display_on();
    }
    
    internal void Disable_all_cams_als()
    {
        _myFPS_camera.enabled = false;
        _myFPS_al.enabled = false;
        
        _map_camera.enabled = false;
        _map_al.enabled = false;

        _my3rdP_camera.enabled = false;
        _my3rdP_al.enabled = false;
        
        _my3rdP_WH.Turn_display_off();
        _myFPS_WH.Turn_display_off();
    }
    
}

public enum CameraViewStatus
{
    player_view,
    map_view,
    thirdP_view,
    no_info
}