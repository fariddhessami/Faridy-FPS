﻿using System;
using System.Collections;
using System.Collections.Generic;
using BeardedManStudios.Forge.Logging;
using BeardedManStudios.Forge.Networking.Generated;
using UnityEngine;

public class PlayerLook : PlayerLookStuffBehavior
{

    [SerializeField] private string mouse_X_input = "Mouse X";
    [SerializeField] private string mouse_Y_input = "Mouse Y";
    
    //why do we do Serialize a Field??
    //Waiting for an answer...

    [SerializeField] private float mouseSensitivity = 100;

    public Transform playerBody;

    private PlayerHealth playerHealth;

    [SerializeField] private Transform _spineTransform;
    private float _camYRotationAccForSpine = 0f;

    void Awake()
    {
        LockCursorCenter();
        playerHealth = GetComponentInParent<PlayerHealth>();
    }

    
    void Update()
    {
        CameraRotation();

        if (Input.GetKeyDown(KeyCode.Slash))
        {
            if (Cursor.lockState == CursorLockMode.Locked)
            {
                UnLockCursorCenter();
            }
            else
            {
                LockCursorCenter();
            }
        }
        
    }

    void LateUpdate()
    {
        if(networkObject == null){return;}
            
        
        if (networkObject.IsOwner)
        {
            _spineTransform.transform.Rotate(Vector3.right * (-_camYRotationAccForSpine));
            // Debug.Log("spine rot : " + _spineTransform.transform.rotation);
        }
        else
        {
            _spineTransform.transform.rotation = networkObject.playerCameraDirection;
        }
    }


    

    private void LockCursorCenter()
    {
        Cursor.lockState = CursorLockMode.Locked;
        //locks curser to the center of the screen or window
        Cursor.visible = false;
        
        BMSLog.Log($" Cursor.lockState : {Cursor.lockState}");
    }
    
    private void UnLockCursorCenter()
    {
        Cursor.lockState = CursorLockMode.None;
        //locks curser to the center of the screen or window
        Cursor.visible = true;
        
        BMSLog.Log($" Cursor.lockState : {Cursor.lockState}");
    }

    
    private float cam_Y_rotation_acc = 0;
    
    private void CameraRotation()
    {

        if (networkObject == null)
        {
            BMSLog.Log($"this is the frame which PlayerLook is null , I'm gonna do nothing");
            return;
        }
        
        if (playerHealth.Life_status() != Life_Status_enum.alive)
        {
            //lock yourself !!
            return;
        }
        
        
        if (networkObject.IsOwner)
        {
            //mouse inputs get recieved

            float cam_X_rotation = Input.GetAxis(mouse_X_input)*Time.deltaTime*mouseSensitivity;
            float cam_Y_rotation = Input.GetAxis(mouse_Y_input)*Time.deltaTime*mouseSensitivity;
        
        
            if (cam_Y_rotation_acc + cam_Y_rotation >= 90)
            {
                cam_Y_rotation = 90 - cam_Y_rotation_acc;
            }
            else if (cam_Y_rotation_acc + cam_Y_rotation <= -90)
            {
                cam_Y_rotation = -90 - cam_Y_rotation_acc;
            }
        
            cam_Y_rotation_acc += cam_Y_rotation;
            cam_Y_rotation_acc = Mathf.Clamp(cam_Y_rotation_acc, -90f, 90f);

            _camYRotationAccForSpine = cam_Y_rotation_acc;

            if (cam_Y_rotation != 0f)
            {
                this.transform.Rotate(Vector3.right * (-cam_Y_rotation));
                networkObject.playerCameraDirection = this.transform.rotation;
                
                
            }
        

            playerBody.Rotate(Vector3.up * (cam_X_rotation));
            networkObject.playerBodyDirection = playerBody.transform.rotation;
            
        }
        else
        {
            //(!networkObject.IsOwner)
            this.transform.rotation = networkObject.playerCameraDirection;
            playerBody.transform.rotation = networkObject.playerBodyDirection;
        }
    }
    
    
    
    
}//PlayerLook
