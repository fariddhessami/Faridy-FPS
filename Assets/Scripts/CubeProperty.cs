﻿using System.Collections;
using System.Collections.Generic;
using BeardedManStudios.Forge.Networking.Generated;
using UnityEngine;

public class CubeProperty : MonoBehaviour
{

    [SerializeField] private CubeLifeStatus _status { get; set; }


    // Start is called before the first frame update
    void Start()
    {
        _status = CubeLifeStatus.dead_Cube;
    }

    // Update is called once per frame
    void Update()
    {
        // if (networkObject.IsOwner)
        // {
        //     networkObject.intId = (int) networkObject.MyPlayerId;
        //     // Debug.Log("I'm sending : " + networkObject.MyPlayerId);
        // }
        // else
        // {
        //     // Debug.Log("I'm recieving : " + networkObject.intId);
        // }
    }
}


enum CubeLifeStatus
{
    alive_Cube,
    dead_Cube
}
