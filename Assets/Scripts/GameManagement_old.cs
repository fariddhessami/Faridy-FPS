﻿using System.Collections;
using System.Collections.Generic;
using BeardedManStudios.Forge.Logging;
using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Generated;
using BeardedManStudios.Forge.Networking.Unity;
using UnityEngine;

public class GameManagement_old : GameManagerStuffBehavior
{
    public static GameManagement_old _instance { get; private set; }
    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
            //why we do this line ??
            //cause we dont want to destroy this object
        }
        else
        {
            Destroy(gameObject);
            //if second is going to be created it shouldn't 
        }
    }
    
    
    [SerializeField] private string jump_input = "Jump";
    [SerializeField] private string respawn_input;



    [SerializeField] private GameObject player_Prefab;

    [SerializeField] private int player_index_counter = 1;
    //this field is for the server
    [SerializeField] private int session_id = 1;
    //for server must be 1 for others must be subsequent numbers


    [SerializeField] private GameObject[] players;
    [SerializeField] private PlayerHealth[] players_Health;
    [SerializeField] private PlayerMove[] players_Move;
    [SerializeField] private Camera[] players_Cameras;
    [SerializeField] private AudioListener[] players_ALs;

    
    public GameObject player_1;
    private PlayerHealth playerHealth;
    private PlayerMove playerMove;

    public GameObject map;

    public GameObject[] spawnPoints;
    

    public Camera fps_camera, map_camera;
    public AudioListener fps_aListener, map_aListener;



    
    void Start()
    {
        spawnPoints = GameObject.FindGameObjectsWithTag("SpawnPoint");
        map = GameObject.FindGameObjectWithTag("Map");
        
        map_camera = map.GetComponentInChildren<Camera>();
        map_aListener = map.GetComponentInChildren<AudioListener>();
        
        map_camera.enabled = true;
        map_aListener.enabled = true;

        players = new GameObject[10];
        players_Cameras = new Camera[10];
        players_Health = new PlayerHealth[10];
        players_Move = new PlayerMove[10];
        players_ALs = new AudioListener[10];
    }

    
    void Update()
    {
        
        int this_is_player_GM = (int) networkObject.MyPlayerId;

        for (int i = 0; i < 11; i++)
        {
            
        }//for all players loop
        
        
        if (players[this_is_player_GM] == null)
        {
            if (Input.GetButtonDown(jump_input))
            {
                // players[this_is_player_GM] = Instantiate(player_Prefab,
                //     new Vector3(0,0,0),player_Prefab.transform.rotation);

                
                players[this_is_player_GM] = NetworkManager.Instance.InstantiatePlayerMoveStuff().gameObject;
                BMSLog.Log($"where are other game managers this is id : {networkObject.MyPlayerId}");
                
                GetPlayer_Components(this_is_player_GM);
                
                SpawnWorks(this_is_player_GM);
                // Spawn_to_Rand_Point(this_is_player_GM);
                // players_Health[this_is_player_GM].Spawn_health();
                
            }
            
        }
        else
        {
            if (players_Health[this_is_player_GM].Life_status() == Life_Status_enum.dead)
            {
                switch_camera_to_map(this_is_player_GM);
                if (Input.GetButtonDown(jump_input))
                {
                    SpawnWorks(this_is_player_GM);
                    // Spawn_to_Rand_Point(this_is_player_GM);
                    // players_Health[this_is_player_GM].Spawn_health();
                }
            }else if (players_Health[this_is_player_GM].Life_status() == Life_Status_enum.alive)
            {
                switch_camera_to_player(this_is_player_GM);
            }   
        }
        
        
    }

    private void GetPlayer_Components(GameObject player)
    {
        playerHealth = player.GetComponent<PlayerHealth>();
        playerMove = player.GetComponent<PlayerMove>();

        fps_camera = player.GetComponentInChildren<Camera>();
        fps_aListener = player.GetComponentInChildren<AudioListener>();
    }

    private void GetPlayer_Components(int playe_id)
    {
        players_Health[playe_id] = players[playe_id].GetComponent<PlayerHealth>();
        players_Move[playe_id] = players[playe_id].GetComponent<PlayerMove>();

        players_Cameras[playe_id] = players[playe_id].GetComponentInChildren<Camera>();
        players_ALs[playe_id] = players[playe_id].GetComponentInChildren<AudioListener>();
    }
    

    void Spawn_to_Rand_Point() // pick a spawn ponit
    {
        int rand = Mathf.FloorToInt(Random.Range(0f, spawnPoints.Length));
        Debug.Log("rand is : " + rand);
        playerMove.Spawn_player_transform(spawnPoints[rand].transform.position,spawnPoints[rand].transform.rotation);
        //handling actual camera rotation is a bit tricky at this point
    }//this is the old one
    
    void Spawn_to_Rand_Point(int player_id) // pick a spawn ponit
    {
        int rand = Mathf.FloorToInt(Random.Range(0f, spawnPoints.Length));
        Debug.Log("rand is : " + rand);
        players_Move[player_id].Spawn_player_transform(spawnPoints[rand].transform.position,spawnPoints[rand].transform.rotation);
        //handling actual camera rotation is a bit tricky at this point
    }

    void switch_camera_to_player()
    {
        fps_camera.enabled = true;
        fps_aListener.enabled = true;
        
        map_camera.enabled = false;
        map_aListener.enabled = false;
    }
    void switch_camera_to_map()
    {
        fps_camera.enabled = false;
        fps_aListener.enabled = false;
        
        map_camera.enabled = true;
        map_aListener.enabled = true;
    }
    void switch_camera_to_player(int player_id)
    {
        players_Cameras[player_id].enabled = true;
        players_ALs[player_id].enabled = true;
        
        map_camera.enabled = false;
        map_aListener.enabled = false;
    }
    void switch_camera_to_map(int player_id)
    {
        players_Cameras[player_id].enabled = false;
        players_ALs[player_id].enabled = false;
        
        map_camera.enabled = true;
        map_aListener.enabled = true;
    }

    void SpawnWorks(int player_id)
    {
        Spawn_to_Rand_Point(player_id);
        players_Health[player_id].RefillHealthMakeAlive();
    }
    
}
