﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManagement : MonoBehaviour
{
    public Camera map_Camera, fps_Camera;
    
    [SerializeField] private string changeCamera_input = "Change camera";
    
    // Start is called before the first frame update
    void Start()
    {
        fps_Camera.enabled = true;
        map_Camera.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (fps_Camera != null)
        {
            if (Input.GetButtonDown(changeCamera_input))
            {
                fps_Camera.enabled = !fps_Camera.enabled;
                map_Camera.enabled = !map_Camera.enabled;
            }
        }
        else
        {
            map_Camera.enabled = !map_Camera.enabled;
        }
        

    }
    
    
}
