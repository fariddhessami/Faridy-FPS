﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BeardedManStudios.Forge.Logging;
using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Generated;
using BeardedManStudios.Forge.Networking.Unity;
using UnityEngine;
using Random = UnityEngine.Random;

public class GameManagement : GameManagerStuffTestBehavior
{

    //    Dynamic list of players
    [SerializeField] private 
        Dictionary<int, PlayerMoveStuffBehavior> _playersList_old;
    [SerializeField] private 
        List<PlayerListElement> _playersList;

    
    //    playerCount
    public static int MaximumPlayerCount = 16;
    
    //    Player Id stuff
    private int _playerIdCount = 0;


    //    variables for myPlayer and camDirector :
    [SerializeField] private CameraDirector _myPlayerCamDirector;
    [SerializeField] private GameObject _myPlayer;
    [SerializeField] private PlayerHealth _myPlayerHealth;
    [SerializeField] private int _wishViewIndex;
    
    
    //    variables for Map and ReSpawn :
    [SerializeField] private GameObject map;
    [SerializeField] private GameObject[] spawnPoints;
    
    
    

    //    singleTone stuff:
    public static GameManagement _instance {  get; private set; }
    public static GameManagement Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<GameManagement>();
            }
            return _instance;
        }
    }
    
    //    player ui
    [SerializeField] private GameObject _uIprefab;
    private PlayerUIScript myPlayerUI;
    
    
    //    Voxel Chunks
    [SerializeField] private GameObject[] _chunkPlaceMents;
    
    //    Round Manager
    [SerializeField] private RoundManagement _roundManager;

    
    //    Reward Values
    [SerializeField] private static int KillReward = 100;
    
    
    void Awake()
    {
        
    }
    
    void Start()
    {
        // Old_start();
    }


    void Old_start()
    {
        _playersList_old = new Dictionary<int, PlayerMoveStuffBehavior>();
        
        _playersList = new List<PlayerListElement>();
        
        _myPlayer = NetworkManager.Instance.InstantiatePlayerMoveStuff().gameObject;

        Inisialize_vals();
        
        
        //    now we start the round
        

        RoundManagement.Instance.AmIServer = 
            networkObject.IsServer;
        RoundManagement.Instance.StartTheRound();
        
        //    if server then start the round if not request synch

        if (networkObject.IsServer)
        {
            // start a round ?
        }
        else
        {
            MakeSynchRequest();
        }
    }
    
    protected override void NetworkStart()
    {
        base.NetworkStart();
        
        Old_start();

        
        if (networkObject.IsServer)
        {
            GetChunkPlaceMentsANDPlaceThem();
            
            RoundManagement.Instance.SetRoundManagerToBeServer();
        }
    }

    public CameraDirector GetMyPlayerCamDirector()
    {
        return _myPlayerCamDirector;
    }
    
    private void GetChunkPlaceMentsANDPlaceThem()
    {
        _chunkPlaceMents = GameObject.FindGameObjectsWithTag("ChunkPlaceMent");

        

        foreach (var place in _chunkPlaceMents)
        {
            // Debug.Log(place.transform.name + " : " + place.transform.position);
            NetworkManager.Instance.InstantiateChunkStuff(0,place.transform.position);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(networkObject == null)
            return;
        
        
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            LogThePlayersList();
        }
        
        
        
        ServiceMyPlayerCamera();
        
    }

    public void AddPlayerToTheList(PlayerMove playerToAdd,int id)
    {
        PlayerListElement newPlayerListElement =
            new PlayerListElement(playerToAdd, id);
        
        _playersList.Add(newPlayerListElement);
        ScoreBoardManagement.Instance.AddPlayerToScoreBoard(newPlayerListElement);
    }

    private void ServiceMyPlayerCamera()
    {
        if (_myPlayerHealth == null)
        {
            // _myPlayerCamDirector.Switch_camera_to_map();
            return;
        }
        
        
        if (_myPlayerHealth.Life_status() == Life_Status_enum.alive)
        {
            switch (_wishViewIndex)
            {
                case 0 :
                    _myPlayerCamDirector.Switch_camera_to_myPlayer();
                    break;
                case 1 :
                    _myPlayerCamDirector.Switch_camera_to_map();
                    break;
                case 2 :
                    _myPlayerCamDirector.Switch_camera_to_3rdPerson();
                    break;
            }

            if (Input.GetKeyDown(KeyCode.P))
            {
                CycleCameraView();
                BMSLog.Log("camera was : "+ _myPlayerCamDirector._CameraViewStatus);
            }
            
        }else if(_myPlayerHealth.Life_status() == Life_Status_enum.dead)
        {
            _myPlayerCamDirector.Switch_camera_to_map();
        }
    }

    private void CycleCameraView()
    {
        _wishViewIndex += 1;
        if (_wishViewIndex == 3)
        {
            _wishViewIndex = 0;
        }
    }

    private void LogThePlayersList()
    {
        BMSLog.Log($"_playersList :");
        foreach (PlayerListElement playerLiEl in _playersList)
        {
            BMSLog.Log(
                "player : " + playerLiEl.PlayerMove.GetPlayerName() +
                "    scores : " + playerLiEl.Score +
                "    id : " + playerLiEl.Id);
        }
    }

    void Inisialize_vals()
    {
        spawnPoints = GameObject.FindGameObjectsWithTag("SpawnPoint");
        map = GameObject.FindGameObjectWithTag("Map");
     
        _myPlayerCamDirector = GetComponentInChildren<CameraDirector>();
        
        // _myPlayerCamDirector._myFPS_camera = _myPlayer.GetComponentInChildren<Camera>();
        // _myPlayerCamDirector._myFPS_al = _myPlayer.GetComponentInChildren<AudioListener>();
        
        
        Camera[] cameras = _myPlayer.GetComponentsInChildren<Camera>();
        AudioListener[] audioListeners = _myPlayer.GetComponentsInChildren<AudioListener>();

        foreach (Camera cam in cameras)
        {
            BMSLog.Log(cam.name);

            if (cam.tag.Equals("FPS_camera"))
            {
                _myPlayerCamDirector._myFPS_camera = cam;
            }else if (cam.tag.Equals("ThirdP_camera"))
            {
                _myPlayerCamDirector._my3rdP_camera = cam;
            }
            else
            {
                BMSLog.Log("unknown camera detected");
            }
            
        }
        foreach (AudioListener al in audioListeners)
        {
            // Debug.Log(al.name);

            if (al.tag.Equals("FPS_camera"))
            {
                _myPlayerCamDirector._myFPS_al = al;
            }else if (al.tag.Equals("ThirdP_camera"))
            {
                _myPlayerCamDirector._my3rdP_al = al;
            }
            else
            {
                BMSLog.Log("unknown al detected");
            }
            
        }

        
        //setting 3rdPerson model in local camera Director :
        _myPlayerCamDirector.My3RdPersonModel =
            _myPlayer.GetComponent<PlayerProperties>().Player3RdPModel;
        _myPlayerCamDirector.My3RdPersonSkinedMesh =
            _myPlayer.GetComponent<PlayerProperties>().Player3RdPModel
            .GetComponentInChildren<SkinnedMeshRenderer>();
        
        // if (_myPlayerCamDirector.My3RdPersonSkinedMesh != null)
        // {
        //     Debug.Log("hehe boii : " + _myPlayerCamDirector.My3RdPersonSkinedMesh.transform.name);
        // }

        _myPlayerCamDirector._myFPS_WH =
            _myPlayerCamDirector._myFPS_camera.GetComponentInChildren<WeaponHolder>();

        _myPlayerCamDirector._my3rdP_WH = _myPlayerCamDirector._myFPS_WH._3rdPersonRef;

        
        
        _myPlayerCamDirector._map_camera = map.GetComponentInChildren<Camera>();
        _myPlayerCamDirector._map_al = map.GetComponentInChildren<AudioListener>();

        
        _myPlayerHealth = _myPlayer.GetComponent<PlayerHealth>();

        _myPlayerCamDirector.Switch_camera_to_myPlayer();
        _wishViewIndex = 0;



        myPlayerUI = Instantiate(_uIprefab).GetComponent<PlayerUIScript>();
        myPlayerUI.Initialize(_myPlayer);
        // myPlayerUiCanvas
        

    }

    

    

    public override void RequestPlayerRespawn(RpcArgs args)
    {
        //so here we serve a player's ReSpawn request
        
        if (networkObject.IsServer)
        {
            int requestedPlayerId = args.GetNext<int>();
            // BMSLog.Log("server received ReSpawn Request for player id : " + requestedPlayerId);
            
            int rand = Mathf.FloorToInt(Random.Range(0f, spawnPoints.Length));
            // Debug.Log($"rand spawnPoint : {rand} is selected for player : {requestedPlayerId} ");


            Vector3 spawnPointPos = spawnPoints[rand].transform.position;
            Quaternion spawnPointRot = spawnPoints[rand].transform.rotation;

        }
    }

    public override void LocalReSpawnService(RpcArgs args)
    {
        BMSLog.Log("well well ... local respawn service here ... ");
        
        
    }

    public override void UpdateRoundTime(RpcArgs args)
    {
        // don't need this
        throw new NotImplementedException();
    }

    public void MakeSynchRequest()
    {
        networkObject.SendRpc(
            RPC_REQ_SYNCH_GAME_INFO, Receivers.Server);
    }
    
    public override void ReqSynchGameInfo(RpcArgs args)
    {
        //    this is server's process of request for synch request

        Debug.Log("now process RS server");
        
        if (networkObject.IsServer)
        {
            RoundState currentRoundState = RoundManagement.Instance._roundState;
            float currentRoundTime = RoundManagement.Instance.GetRemainingTimeFloat();
            int currentRS = RoundManagement.RoundStateToInt(currentRoundState);

            //    now tell the everyone else :
            
             networkObject.SendRpc(
                 RPC_RES_SYNCH_GAME_INFO, Receivers.Others,
                 currentRS, currentRoundTime);
        }
        else
        {
            throw new Exception
            ("server should process this request");
        }
    }

    public override void ResSynchGameInfo(RpcArgs args)
    {
        //    this is client's process of response for synch request
        
        Debug.Log("now process RS client");
        
        int currentRS = args.GetNext<int>();
        float currentRoundTime = args.GetNext<float>();
        
        RoundState currentRoundState = RoundManagement.IntToRoundState(currentRS);
        
        //    now updating the server info
        RoundManagement.Instance.UpdateGameInfoByServerSynch
            (currentRoundState,currentRoundTime);
    }

    public override void ResRewardPlayerById(RpcArgs args)
    {
        int killerId = args.GetNext<int>();
        int reward = args.GetNext<int>();
        
        RewardPlayerById(killerId,reward);
        CountOneKill(_playersList[killerId].PlayerMove);
    }


    public bool DoesLocalGMcontainsId(int id)
    {
        return _playersList_old.ContainsKey(id);
    }

    public GameObject SelectRandomResPawnPoint()
    {
        int rand = Mathf.FloorToInt(Random.Range(0f, spawnPoints.Length));
        return spawnPoints[rand];
    }


    public PlayerListElement[] GetArrayOfPlayerList()
    {
        return _playersList.ToArray();
    }

    public String GetPlayerNameById(int id)
    {
        if (id < 0)
        {
            return "player id was : " + id;
        }
        if (_playersList[id] != null)
        {
            return _playersList[id].PlayerMove.GetPlayerName();
        }
        else
        {
            return "no player found!";
        }
    }
    
    public void RewardPlayer(PlayerMove playerMove)
    {
        
        //    Later we should synchronize an index to each player not to do
        //    these kind of things
        
        // Debug.Log(
        //     "this is gameManager and I'm gonna reward player :"
        //     + playerMove.GetPlayerName());


        
        foreach (PlayerListElement element in _playersList)
        {
            if (element.PlayerMove == playerMove)
            {
                
                element.UpdateScores(element.Score+1);
            }
        }
    }

    private void RewardPlayerById(int playerId,int rewardValue)
    {
        //    this is done locally
        //    id should be checked before
        
        _playersList[playerId].UpdateScores
            (_playersList[playerId].Score+rewardValue);
    }
    
    public void ProcessRewardPlayerForKill(int killerId)
    {
        //    server tells everyOne to reward the player
        //    for the kill, also counts one kill for the killer player
        
        if (networkObject.IsServer)
        {
            networkObject.SendRpc(
                RPC_RES_REWARD_PLAYER_BY_ID, Receivers.AllBuffered,
                killerId,KillReward);
        }
        else
        {
            throw new Exception
                ("server should make this reward process");
        }
    }

    public int GeneratePlayerId()
    {
        if (networkObject.IsServer)
        {
            int result = _playerIdCount;
            _playerIdCount++;
            return result;
        }
        else
        {
            throw new Exception("only server can generate player ID!");
        }
    }

    public void CountOneDie(PlayerMove playerMove)
    {
        foreach (PlayerListElement element in _playersList)
        {
            if (element.PlayerMove == playerMove)
            {
                element.Deaths = element.Deaths+1;
            }
        }
    }
    
    public void CountOneKill(PlayerMove playerMove)
    {
        foreach (PlayerListElement element in _playersList)
        {
            if (element.PlayerMove == playerMove)
            {
                element.Kills = element.Kills+1;
            }
        }
    }


    public void ChangeRoundProcessByServer()
    {
        RoundState exRoundState = RoundManagement.Instance._roundState;
        RoundState nextRoundState;

        switch (exRoundState)
        {
            case RoundState.Finished :
                nextRoundState = RoundState.OnGoing; break;
            default :
                nextRoundState = RoundState.Finished; break;
        }

        //    change the round for the server
        RoundManagement.Instance.
            UpdateGameInfoByServerSynch
                (nextRoundState,
                RoundManagement.DurationByRoundState(nextRoundState));
        
        //    synch game info for everyone else
        MakeSynchRequest();
    }
}

public class PlayerListElement
{
    public PlayerMove PlayerMove;
    public int Score;
    public int Id;
    public int Kills;
    public int Deaths;

    public PlayerListElement(PlayerMove pm,int id)
    {
        PlayerMove = pm;
        Score = 0;
        Id = id;
        Kills = 0;
        Deaths = 0;
    }

    public void UpdateScores(int sc)
    {
        Score = sc;
        // Debug.Log("scores updated to " + Score);
    }
}