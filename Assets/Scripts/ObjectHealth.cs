﻿using System.Collections;
using BeardedManStudios.Forge.Logging;
using System.Collections.Generic;
using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Generated;
using UnityEngine;

public class ObjectHealth : ObjectHealthStuffBehavior
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void ReqChangeHp(RpcArgs args)
    {
        float hpChangeAmount = args.GetNext<float>();

        if (networkObject.IsServer)
        {
            BMSLog.Log("server recieved request for hp change : " + hpChangeAmount);
            networkObject.SendRpc(RPC_RES_CHANGE_HP, Receivers.AllBuffered,hpChangeAmount);
        }
        else
        {
            // a client only requests to change,
            // nothing for him to do here   
        }
    }

    public override void ResChangeHp(RpcArgs args)
    {
        throw new System.NotImplementedException();
    }
}
