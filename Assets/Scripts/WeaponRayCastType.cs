﻿using System;
using System.Collections;
using System.Collections.Generic;
using BeardedManStudios.Forge.Logging;
using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Generated;
using UnityEngine;
using UnityEngine.Audio;

[System.Serializable]
public class WeaponRayCastType : WeaponRayCastTypeStuffBehavior
{
    public AudioClip fire_Clip;
    [SerializeField] private AudioSource _weaponAudioSource;
    
    
    
    public bool IsEffectsShown = true;
    
    public float damagePerShot = 10f;
    public float rangeCap = 100f;

    public Camera fpsCam;

    [SerializeField] private ParticleSystem _muzzleFlashPartSys;

    [SerializeField] private Animator _weaponAnimator;
    
    [SerializeField] private float fireDelay;

    private float nextTimeToShoot = 0f;
    
    [SerializeField] private WeaponHolder _weaponHolder;
    
    // Start is called before the first frame update
    void Start()
    {
        
        
        
        _weaponHolder = this.transform.parent.GetComponent<WeaponHolder>();
        _weaponAudioSource.clip = fire_Clip;
    }

    // Update is called once per frame
    void Update()
    {
        // Debug.Log("this is Update for weapon");


        
        
        if (networkObject == null)
        {
            //    waiting on behalf of WeaponRayCastType
            return;
        }

        if (networkObject.IsOwner && (!_weaponHolder.isThirdPerson()))
        {
            if (Input.GetButton("Fire1") && Time.time>= nextTimeToShoot)
            {
                nextTimeToShoot = Time.time + fireDelay;
                
                Vector3 startPos = fpsCam.transform.position;
                Vector3 direction = fpsCam.transform.forward;
                networkObject.SendRpc(RPC_REQ_SHOOT, Receivers.Server,
                    startPos,
                    direction);
                
            } 
        }
    }

    void FixedUpdate()
    {
        // Debug.Log("this is FixedUpdate for weapon");
        // _weaponAnimator.SetBool("fire",false);
        
        
        AnimatorStateInfo animatorInfo = _weaponAnimator.GetCurrentAnimatorStateInfo(0);
        
        // Debug.Log("animatorInfo.IsName(fire);"+ animatorInfo.IsName("fire"));
        
        if (animatorInfo.IsName("fire_state"))
        {
            _weaponAnimator.SetBool("fire",false);
        }

    }
    
    
    public void ShootJustEffect()
    {
        //    FIRE JUST EFFECT


        if (IsEffectsShown)
        {
            _muzzleFlashPartSys.Play();
            _weaponAudioSource.Play();
        }
    }
    

    
    private void ShootWeaponServerPOV(Vector3 startPos,Vector3 direction)
    {
        
        
        RaycastHit hitInfo;
        bool didHit = Physics.Raycast(startPos,
            direction,
            out hitInfo,
            rangeCap);

        if (didHit)
        {
            // Debug.Log(hitInfo.transform.name);

            PlayerHealth targetedPlayer = hitInfo.transform.GetComponent<PlayerHealth>();
            if (targetedPlayer != null)
            {
                // Debug.Log("sb is shot!!!!XXXXXXXXXXXXXXXXX");

                int shooterId = 
                    _weaponHolder.GetOwnerPlayerMove().GetPlayerId();
                
                targetedPlayer.MakeReqChangeHp(-damagePerShot,shooterId);


                if (targetedPlayer.networkObject.IsOwner)
                {
                    // Debug.Log("u've shot YOURSELF!!!!!!!!!!!!$$$$$$$$");
                }
            }else if (hitInfo.transform.tag.Equals("VoxelChunk"))
            {

                // Debug.Log("your hit tri index : " + hitInfo.triangleIndex);
                
                
                Chunk targetedChunk = hitInfo.transform.GetComponent<Chunk>();
                Vector3Int vec = targetedChunk.GetXYZ_byTriIndex(hitInfo.triangleIndex);

                // Debug.Log($"targeted cell should be" +
                //           $" x:{vec.x}" +
                //           $" y:{vec.y}" +
                //           $" z:{vec.z}" );
                
                targetedChunk.Server_cubeRemove(vec.x,vec.y,vec.z);
                
                
                //Rewarding stuff

                PlayerMove playerToReward = _weaponHolder.GetOwnerPlayerMove();
                playerToReward.ProcessRewardSelf();

            }
            
        }
        else
        {
            // Debug.Log("your shot was missed!!!!");
        }
        
    }

    public override void ReqShoot(RpcArgs args)
    {
        // only server side :

        Vector3 startPos = args.GetNext<Vector3>();
        Vector3 direction = args.GetNext<Vector3>();

        ShootWeaponServerPOV(startPos,direction);

        networkObject.SendRpc(RPC_RES_SHOOT, Receivers.All,
            startPos,
            direction);
    }

    public override void ResShoot(RpcArgs args)
    {
        // only visualization for shooting

        // Debug.Log("FIRE_RES");
        
        Vector3 startPos = args.GetNext<Vector3>();
        Vector3 direction = args.GetNext<Vector3>();


        ShootJustEffect();
        
        _weaponAnimator.SetBool("fire",true);

        _weaponHolder.Make3rdP_WeaponShootEffect(1);

        // BMSLog.Log("boom!!!!");

    }
}
