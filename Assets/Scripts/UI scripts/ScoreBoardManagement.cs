﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreBoardManagement : MonoBehaviour
{

    [SerializeField] private Image backImage;
    private Text[] _textChildren;
    private Image[] _imageChildren;
    
    
    [SerializeField] private GameObject _rowSample;

    private ScoreBoardRow[] _scoreBoardRowList;
    private PlayerListElement[] _playerListElementList;

    private PlayerListElement[] _sortedPlayerListElementList;

    private bool isContentDisplayed = false;

    private int _playerCount = 0;

    public static ScoreBoardManagement _instance {  get; private set; }
    public static ScoreBoardManagement Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<ScoreBoardManagement>();
            }
            return _instance;
        }
    }

    void Awake()
    {
        _scoreBoardRowList = 
            new ScoreBoardRow[GameManagement.MaximumPlayerCount];
        _playerListElementList =
            new PlayerListElement[GameManagement.MaximumPlayerCount];
    }
    
    void Start()
    {

        
        _textChildren = this.GetComponentsInChildren<Text>();
        _imageChildren = this.GetComponentsInChildren<Image>();
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Tab))
        {
            backImage.enabled = true;
            ChangeDisplayContent(true);
            isContentDisplayed = true;
        }else if(Input.GetKeyUp(KeyCode.Tab))
        {
            backImage.enabled = false;
            ChangeDisplayContent(false);
            isContentDisplayed = false;
        }
        else
        {
            if (isContentDisplayed)
            {
                // UpdateContent();
                UpdateContent2();
            }            
        }
    }

    private void UpdateContent()
    {
        int maxPlayerCount = GameManagement.MaximumPlayerCount;
        
        for (int i = 0; i < maxPlayerCount; i++)
        {
            if (_playerListElementList[i] != null)
            {
                _scoreBoardRowList[i]._score.text =
                    _playerListElementList[i].Score.ToString();
                _scoreBoardRowList[i]._kills.text =
                    _playerListElementList[i].Kills.ToString();
                _scoreBoardRowList[i]._deaths.text =
                    _playerListElementList[i].Deaths.ToString();
            }
        }
    }
    
    private void UpdateContent2()
    {
        int maxPlayerCount = GameManagement.MaximumPlayerCount;

        _sortedPlayerListElementList = new PlayerListElement[_playerCount];
        
        for (int i = 0; i < _playerCount; i++)
        {
            _sortedPlayerListElementList[i] = _playerListElementList[i];
        }
        
        Array.Sort(_sortedPlayerListElementList,
            delegate(PlayerListElement user1, PlayerListElement user2) {
            return user2.Score.CompareTo(user1.Score);
        });
        
        for (int i = 0; i < _playerCount; i++)
        {
            if (_sortedPlayerListElementList[i] != null)
            {
                _scoreBoardRowList[i]._score.text =
                    _sortedPlayerListElementList[i].Score.ToString();
                _scoreBoardRowList[i]._kills.text =
                    _sortedPlayerListElementList[i].Kills.ToString();
                _scoreBoardRowList[i]._deaths.text =
                    _sortedPlayerListElementList[i].Deaths.ToString();
                _scoreBoardRowList[i]._name.text =
                    _sortedPlayerListElementList[i].PlayerMove.GetPlayerName();
                _scoreBoardRowList[i]._id.text =
                    _sortedPlayerListElementList[i].Id.ToString();
            }
        }
    }

    void ChangeDisplayContent(bool changeTo)
    {


        foreach (Text text in _textChildren)
        {
            text.enabled = changeTo;
        }
        foreach (Image image in _imageChildren)
        {
            image.enabled = changeTo;
        }
        foreach (ScoreBoardRow scBrdRow in _scoreBoardRowList)
        {
            if (scBrdRow != null)
            {
                scBrdRow._name.enabled = changeTo;
                scBrdRow._id.enabled = changeTo;
                scBrdRow._score.enabled = changeTo;
                scBrdRow._backImage.enabled = changeTo;
                scBrdRow._kills.enabled = changeTo;
                scBrdRow._deaths.enabled = changeTo;
            }
        }
    }

    
    public void AddPlayerToScoreBoard(PlayerListElement newPlayerListElement)
    {
        //    this function is call by GM
        //    when registering new player


        int id = newPlayerListElement.Id;
        PlayerMove playerToAdd = newPlayerListElement.PlayerMove;
        
        
        
        GameObject newRow = Instantiate(_rowSample, 
            _rowSample.transform.position + 
            (id+1)*24*Vector3.down,
            _rowSample.transform.rotation);

        newRow.transform.parent = this.transform;
        
        // Debug.Log("newRow    :" + newRow.transform.name);
        
        ScoreBoardRow scoreBoardRow = newRow.GetComponent<ScoreBoardRow>();
        
        scoreBoardRow.UpdateName(playerToAdd.GetPlayerName());
        scoreBoardRow.UpdateId(id);
        
        scoreBoardRow.UpdateDeaths(newPlayerListElement.Deaths);
        scoreBoardRow.UpdateKills(newPlayerListElement.Kills);

        _scoreBoardRowList[id] = newRow.GetComponent<ScoreBoardRow>();
        _playerListElementList[id] = newPlayerListElement;

        _playerCount++;
    }

}
