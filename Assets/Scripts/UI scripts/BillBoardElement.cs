﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using UnityEngine;
using UnityEngine.UI;

public class BillBoardElement : MonoBehaviour
{
    private CameraDirector _cameraDirector;
    [SerializeField] private Camera activeCam;
    
    [SerializeField] private Text text;


    void Awake()
    {
    }
    
    void Start()
    {
        // _cameraDirector = 
        //     GameManagementTest.Instance.GetMyPlayerCamDirector();
    }
    
    void Update()
    {
    }

    // after all the movements are done
    void LateUpdate()
    {
        
        if (_cameraDirector==null)
        {
            Debug.Log("alo");
            _cameraDirector = 
                GameManagement.Instance.GetMyPlayerCamDirector();
        }
        
        activeCam = _cameraDirector.GetActiveCamera();
        
        if(activeCam==null)    return;
        
        transform.LookAt(
            transform.position +
            activeCam.transform.rotation * Vector3.forward,
            activeCam.transform.rotation * Vector3.up);
    }

    public void UpdatePlayerNameTag(String nameTag)
    {
        text.text = nameTag;
    }
    
}
