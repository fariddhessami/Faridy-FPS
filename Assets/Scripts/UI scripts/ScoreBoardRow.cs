﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreBoardRow : MonoBehaviour
{

    [SerializeField] public Text _name;
    [SerializeField] public Text _score;
    [SerializeField] public Text _id;
    [SerializeField] public Image _backImage;
    [SerializeField] public Text _kills;
    [SerializeField] public Text _deaths;
    
    private ScoreBoardRow _scBoardRow;
    
    
    void Awake()
    {
        _scBoardRow = this.GetComponent<ScoreBoardRow>();
    }
    
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
 
    }


    public void UpdateName(String str)
    {
        _name.text = str;
    }
    
    public void UpdateScore(int sc)
    {
        _score.text = sc.ToString();
    }
    
    public void UpdateId(int id)
    {
        _id.text = id.ToString();
    }
    
    public void UpdateKills(int k)
    {
        _kills.text = k.ToString();
    }
    
    public void UpdateDeaths(int d)
    {
        _deaths.text = d.ToString();
    }

    public ScoreBoardRow getScoreBoardRow()
    {
        return _scBoardRow;
    }
}
