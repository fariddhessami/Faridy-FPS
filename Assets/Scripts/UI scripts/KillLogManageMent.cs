﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KillLogManageMent : MonoBehaviour
{

    private Queue<String> _killFeedStack;
    [SerializeField] private Text _killFeedText;
    
    
    public static KillLogManageMent _instance {  get; private set; }
    public static KillLogManageMent Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<KillLogManageMent>();
            }
            return _instance;
        }
    }
    
    
    
    void Awake()
    {
        _killFeedStack = new Queue<string>();
    }

    
    void Update()
    {
        
    }

    public void AddLineToKillFeed(int killer, int playerKilled)
    {
        String killerName = 
            FigureOutNameForId(killer);
        
        String killedName = 
            FigureOutNameForId(playerKilled);
        
        String result =
            killerName + " killed " + killedName;

        AddStringLineToKillFeed(result);
    }

    private String FigureOutNameForId(int id)
    {
        switch (id)
        {
            case -1 : return "DeathFloor";
            default : return 
                GameManagement.Instance.GetPlayerNameById(id);
        }
    }

    private void AddStringLineToKillFeed(String line)
    {
        _killFeedStack.Enqueue(line);
        if (_killFeedStack.Count >= 6)
        {
            _killFeedStack.Dequeue();
        }
        UpdateTextDisplay();
    }

    private void UpdateTextDisplay()
    {
        String result;
        result = "";
        
        foreach (String VARIABLE in _killFeedStack)
        {
            result = result + VARIABLE + "\n";
        }

        _killFeedText.text = result;
    }
}
