﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUIScript : MonoBehaviour
{
    [SerializeField] public Text UiHpHud;

    private PlayerHealth _myPlayerHealth;
    private GameObject _myPlayer;
    
    [SerializeField] private Text _roundTimeText;
    [SerializeField] private Text _roundStatText;

    public void Initialize(GameObject myPlayer)
    {
        _myPlayer = myPlayer;
        _myPlayerHealth = myPlayer.GetComponent<PlayerHealth>();
    }
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        UpdateHUD_hp();
        UpdateRoundInfo();
    }
    
    private void UpdateHUD_hp()
    {
        UiHpHud.text = _myPlayerHealth.getHP().ToString();
    }
    
    private void UpdateRoundInfo()
    {
        _roundTimeText.text = RoundManagement.Instance.GetTimeDisplay();
        _roundStatText.text = RoundManagement.Instance._roundState.ToString();
    }
}
