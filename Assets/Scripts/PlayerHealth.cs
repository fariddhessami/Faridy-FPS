﻿using System;
using System.Collections;
using System.Collections.Generic;
using BeardedManStudios.Forge.Logging;
using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Generated;
using UnityEngine;
using UnityEngine.UI;


public enum Life_Status_enum {alive,dead}

public class PlayerHealth : PlayerHealthStuffBehavior
{
    [SerializeField] private string die_input = "Die";
    [SerializeField] private string hp_change_input = "Hp_change";

    public GameObject playerObject;

    private PlayerMove _playerMove;
    private PlayerLook _playerLook;

    public static float start_Hp = 100f;
    public static float min_Hp = 0f;
    public static float max_Hp = 100f;

    
    
    private CharacterController charController;
    private GameObject[] deathFloors;
    
    [SerializeField] private GameObject playerCubeBottom;


    private float hp;

    [SerializeField] private Life_Status_enum life_Status;
    
    [SerializeField] private Text ui_hp_hud;


    private int _potentialKiller = -2;
    

    void Awake()
    {
        
    }
    
    // Start is called before the first frame update
    void Start()
    {
        
    }


    protected override void NetworkStart()
    {
        base.NetworkStart();
        
        
        Debug.Log("this is start of player health ");
        
        
        hp = min_Hp;
        life_Status = Life_Status_enum.dead;
        
        
        
        //these two line might better be in a spawn() function... right?

        deathFloors = GameObject.FindGameObjectsWithTag("DeathFloor");
        
        charController = GetComponent<CharacterController>();
        _playerMove = playerObject.GetComponent<PlayerMove>();
        _playerLook = playerObject.GetComponentInChildren<PlayerLook>();

        

        // GetHudRefsHealth();
        
    }

    private void GetHudRefsHealth()
    {
        //getting hp hud text:

        var var1 = transform.Find("Canvas");
        var var2 = var1.Find("HP_text");
        if (var2 == null)
        {
            Debug.Log("noooooooooooo2");
        }

        ui_hp_hud = var2.GetComponent<Text>();
        ui_hp_hud.text = "salam hp";
    }

    private void UpdateHUD_hp()
    {
        ui_hp_hud.text = hp.ToString();
    }
    
    void Update()
    {

        
        
        if (networkObject == null)
        {
            // this is the frame which PlayerHealth is null ,
            // I'm gonna do nothing
            
            return;
        }

  
        
        
        
        if (networkObject.IsOwner)
        {
            //update hud for local player
            // UpdateHUD_hp();
        }
        

        if (networkObject.IsOwner)
        {
            SelfDamageCommandByInput();
        }
        


        // if (networkObject.IsServer)
        // {
        //     //only server checks whether a player is dead or not
        //     
        //     death_check_hp();
        // }

        if (networkObject.IsOwner)
        {
            if (life_Status == Life_Status_enum.dead)
            {
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    networkObject.SendRpc(RPC_REQ_RESPAWN, Receivers.Server);
                }
            }
        }
        
        
    }

    private void SelfDamageCommandByInput()
    {
        
        
        if (Input.GetButtonDown(hp_change_input))
        {
            float selfDamageValue;
            int myPlayerId = _playerMove.GetPlayerId();
            
            // networkObject.SendRpc(RPC_REQ_CHANGE_HP, Receivers.Server,
            //     10f * (Input.GetAxis(hp_change_input)));

            selfDamageValue = 10f * (Input.GetAxis(hp_change_input));
            
            MakeReqChangeHp(selfDamageValue,myPlayerId);
            return;
        }

        if (Input.GetButtonDown(die_input))
        {
            float selfDamageValue;
            int myPlayerId = _playerMove.GetPlayerId();
            
            // networkObject.SendRpc(RPC_REQ_CHANGE_HP, Receivers.Server,
            //     -1f * max_Hp);

            selfDamageValue = -1f * max_Hp;
            
            MakeReqChangeHp(selfDamageValue,myPlayerId);
            return;
        }
    }



    public override void Die(RpcArgs args)
    {
        hp = min_Hp;
        life_Status = Life_Status_enum.dead;
        Debug.Log("player is killed by die RPC");
        playerCubeBottom.SetActive(false);

           
        //    counting death and kill locally:
        GameManagement.Instance.CountOneDie(_playerMove);
        int myPlayerId = _playerMove.GetPlayerId();
        KillLogManageMent.Instance.
            AddLineToKillFeed(_potentialKiller, myPlayerId);

        if (networkObject.IsServer)
        {
            if (_potentialKiller<0)
            {
                Debug.Log("killer isn't a player");
                return;
            }
            GameManagement.Instance.ProcessRewardPlayerForKill(_potentialKiller);
        }
    }

    public void MakeReqChangeHp(float hpChangeAmount, int causerId)
    {
        Debug.Log("MakeReqChangeHp is called for : "
                  + hpChangeAmount + " caused by playerID : "
                  + causerId );
        
        networkObject.SendRpc(RPC_REQ_CHANGE_HP, Receivers.Server,
            hpChangeAmount,causerId);
    }
    
    
    public override void ReqChangeHp(RpcArgs args)
    {
        float hpChangeAmount = args.GetNext<float>();
        int causerId = args.GetNext<int>();

        if (networkObject.IsServer)
        {
            networkObject.SendRpc(RPC_RES_CHANGE_HP,
                Receivers.AllBuffered, hpChangeAmount, causerId);
        }
        else
        {
            // a client only requests to change,
            // nothing for him to do here   
        }
    }
    
    public override void ResChangeHp(RpcArgs args)
    {
        float hpChangeAmount = args.GetNext<float>();
        int causerId = args.GetNext<int>();

        Debug.Log($"ResChangeHp changeHP for :" +
                  $" {hpChangeAmount} causer : {causerId}");

        _potentialKiller = causerId;
        TakeChangeForHp(hpChangeAmount);
        
    }

    public override void ReqRespawn(RpcArgs args)
    {
        //    this is SERVER starting to service the reSpawn request
        
        GameObject randReSpawnPoint = GameManagement.Instance.SelectRandomResPawnPoint();
        
        networkObject.SendRpc(RPC_LOCAL_RESPAWN, Receivers.AllBuffered,
            randReSpawnPoint.transform.position,randReSpawnPoint.transform.rotation);
        
    }

    public override void LocalRespawn(RpcArgs args)
    {
        
        Vector3 spawnPointPos = args.GetNext<Vector3>();
        Quaternion spawnPointRot = args.GetNext<Quaternion>();

        Debug.Log("health LocalRespawn");
        
        _playerMove.Spawn_player_transform(spawnPointPos,spawnPointRot);
        RefillHealthMakeAlive();

    }


    public void RefillHealthMakeAlive()
    {
        life_Status = Life_Status_enum.alive;
        hp = start_Hp;
        // Debug.Log("player gained health");
        playerCubeBottom.SetActive(true);
    }
    

    void TakeChangeForHp(float dmg_val)
    {
        Debug.Log("TakeChangeForHp : "+dmg_val);
        
        hp = hp + dmg_val;
        hp = Mathf.Clamp(hp, min_Hp, max_Hp);
        
        if (networkObject.IsServer)
        {
            //only server checks whether a player is dead or not
            
            death_check_hp();
        }
    }

    void death_check_hp()
    {
        if (life_Status == Life_Status_enum.alive)
        {
            if (hp <= min_Hp)
            {
                BMSLog.Log("SERVER DeathChecked : PLAYER IS DEAAAD!!!");
                networkObject.SendRpc(RPC_DIE, Receivers.AllBuffered);
            }
        }
    }

    void Death_floors_check()
    {

        foreach (GameObject de_floor in deathFloors)
        {
            de_floor.GetComponent<Collider>();
        }
    }

    // private void OnCollisionEnter(Collision collision)
    // {
    //     if (networkObject.IsServer)
    //     {
    //         if (collision.gameObject.CompareTag("DeathFloor"))
    //         {
    //             networkObject.SendRpc(RPC_DIE, Receivers.AllBuffered);
    //         }
    //     }
    // }

    public float getHP()
    {
        return hp;
    }

    
    public Life_Status_enum Life_status()
    {
        return life_Status;
    }

    
}