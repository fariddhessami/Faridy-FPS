﻿using System.Collections;
using System.Collections.Generic;
using BeardedManStudios.Forge.Logging;
using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Generated;
using UnityEngine;

public class SphereGuyWorks : GuyStuffBehavior
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            networkObject.SendRpc(RPC_MOVE_UP,Receivers.All);
            networkObject.SendRpc(RPC_JESUS_H,Receivers.All);
        }
        
    }

    public override void MoveUp(RpcArgs args)
    {
        this.transform.position += Vector3.up;
    }

    public override void JesusH(RpcArgs args)
    {
        BMSLog.Log("jesus H !");
    }
}
