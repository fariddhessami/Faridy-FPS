﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer 
{
    public float CountDownValue { private set; get; }
    private bool _isCountingDown;
    
    public bool IsCountDownFinished { private set; get; }
    
    
    public void StartCountDown(float seconds)
    {
        Debug.Log($"timer started for {seconds}");
        Debug.Log(DisplayTime());
        IsCountDownFinished = false;
        _isCountingDown = true;
        CountDownValue = seconds;
    }

    void FinishTimer()
    {
        Debug.Log($"timer finished !");
        Debug.Log(DisplayTime());
        IsCountDownFinished = true;
        _isCountingDown = false;
        CountDownValue = 0.0f;
    }
    
    public void UpdateTimerData(float currentRoundTime)
    {
        Debug.Log($"timer synched for {currentRoundTime}");
        Debug.Log(DisplayTime());
        IsCountDownFinished = false;
        _isCountingDown = true;
        CountDownValue = currentRoundTime;
    }
    
    // void Awake()
    // {
    //     FinishTimer();
    // }
    
    public void UpdateCountDown()
    {
        // Debug.Log("time : " + DisplayTime());
        
        if (_isCountingDown)
        {
            if (CountDownValue.CompareTo(0.0f) <= 0)
            {
                FinishTimer(); 
            }
            else
            {
                CountDownValue -= Time.deltaTime;
            }
        }
    }
    
    public String DisplayTime()
    {
        float minutes = Mathf.FloorToInt(CountDownValue / 60);  
        float seconds = Mathf.FloorToInt(CountDownValue % 60);
        
        return String.Format("{0:00}:{1:00}", minutes, seconds);
    }

    
}
