﻿using System;
using System.Collections;
using System.Collections.Generic;
using BeardedManStudios.Forge.Logging;
using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Generated;
using UnityEngine;

public class PlayerProperties : MonoBehaviour
{
    
    
    [SerializeField] public GameObject Player3RdPModel;    //set this in prefab!!!

    
    [SerializeField] private String _playerName;

    [SerializeField] private BillBoardElement _playerNameTag;
    
    [SerializeField] public int PlayerId{ get; set; }


    [SerializeField] private WeaponHolder FirstPerWh;
    [SerializeField] private WeaponHolder ThirdPerWh;


    void Start()
    {
        //Don't Do network works here!
        
    }

    
    
    void Update()
    {
        
    }

    public void FetchNameFromSettingManager(bool isThisMyPlayer)
    {
        if (!isThisMyPlayer)
        {
            return;
        }
        
        
        String name = SettingManagement.Instance.GetMyPlayerName();
        
        SetNameAndNameTag(name);
    }
    

    public void SetNameAndNameTag(String name)
    {
        _playerName = name;

        _playerNameTag.UpdatePlayerNameTag(name);
    }
    
    public String GetName()
    {
        return _playerName;
    }

    public WeaponHolder GetThirdPerWh()
    {
        return ThirdPerWh;
    }
    public WeaponHolder GetFirstPerWh()
    {
        return FirstPerWh;
    }
    
}
