﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BeardedManStudios.Forge.Logging;
using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Generated;
using BeardedManStudios.Forge.Networking.Unity;
using UnityEngine;

public class GameLogicManagement_training : GameLogicTrainingStuffBehavior
{
    //    singleTone stuff:
    private static GameLogicManagement_training _instance;
    public static GameLogicManagement_training Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<GameLogicManagement_training>();
            }
            return _instance;
        }
    }

    //myPlayer Reference
    private cubeMovementNetBehavior myCube;
    
    //Dynamic list of players
    [SerializeField] private Dictionary<int, cubeMovementNetBehavior> _playersList;

    void Awake()
    {
        
    }
    
    void Start()
    {
        _playersList = new Dictionary<int, cubeMovementNetBehavior>();
        Debug.Log("_playersList count before adding anything to it : "+ _playersList.Count);
        myCube = NetworkManager.Instance.InstantiatecubeMovementNet();
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            BMSLog.Log($"my id is : "+ networkObject.MyPlayerId);
            BMSLog.Log($"_playersList :");

            foreach (var VARIABLE in _playersList)
            {
                BMSLog.Log("var: " + VARIABLE);
            }

            
        }
    }


    public void Check_and_Register(int player_id,cubeMovementNetBehavior playerCube)
    {
        //this method is created to flank the issue of unServiced RPC call for the client side

        if (_playersList.Keys.Contains(player_id))
        {
            BMSLog.Log("adding manually averted");
            return;
        }
        else
        {
            Register_player(player_id, playerCube);
            BMSLog.Log("adding manually proceeded,this is after the adding opeeration");
        }

    }
    
    
    public void Register_player(int player_id,cubeMovementNetBehavior playerCube)
    {
        BMSLog.Log($"GM< so I'm GM id : {networkObject.MyPlayerId} what do we have here?");
        BMSLog.Log($"GM< add key : {networkObject.MyPlayerId} val : {playerCube} ?");
        _playersList.Add(player_id,playerCube);
        BMSLog.Log("GM< so here u go pal and I'm gonna log it : _playersList[player_id] : ");
        BMSLog.Log(_playersList[player_id].ToString());
        BMSLog.Log("GM< zat ziad");
    }
    
    
    public override void SpawnPlayer(RpcArgs args)
    {
        return;
    }
}
