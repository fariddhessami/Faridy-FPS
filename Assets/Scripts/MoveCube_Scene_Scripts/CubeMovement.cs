﻿using System;
using System.Collections;
using System.Collections.Generic;
using BeardedManStudios.Forge.Logging;
using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Generated;
using UnityEngine;

// public class CubeMovement : MonoBehaviour
public class CubeMovement : cubeMovementNetBehavior
{

    private String cube_massage;
        
    void Start()
    {
        BMSLog.Log($"cube started for player :" +
                   $" {networkObject.MyPlayerId} " +
                   $"and named {this} and RPC called from here");

        int ownerID;

        if (networkObject.IsOwner)
        {
            BMSLog.Log("hehe1");
            ownerID = (int)networkObject.MyPlayerId;
            BMSLog.Log("after all, ownerID : "+ ownerID);

            cube_massage = $"this cube is owned by player : {networkObject.MyPlayerId} ";

            networkObject.SendRpc(RPC_REGISTER_U_R_S_E_L_FTO_G_M,Receivers.AllBuffered,
                ownerID,cube_massage);
            
            //seems like the client side doesn't do the RPC instructions for himself, 
            // but it does work smoothly for the server (by the way)!!!!
            
            //so to counter the issue we do this :

            GameLogicManagement_training.Instance.Check_and_Register(ownerID,this);


        }
        else
        {
            BMSLog.Log("hehe2");
            BMSLog.Log("the BMS who owns this object should tell me what to do");
        }

        
    }



    void Update()
    {
            //owner section: (no more server and client)
        if (!networkObject.IsOwner)
        {
            transform.position = networkObject.postiton;
            

        }    //not owner section:
        else
        {
            
            transform.position += new Vector3(
                                      Input.GetAxis("Horizontal"),
                                      Input.GetAxis("Vertical"),
                                      0
                                  ) * Time.deltaTime * 5.0f;
            networkObject.postiton = transform.position;
            
        }
        
        // BMSLog.Log("transform.position");
    }


    public override void RegisterURSELFtoGM(RpcArgs args)
    {
        BMSLog.Log("**this is the method where we service the RPC");
        
        int playerID = args.GetNext<int>();
        //so this wasn't even needed right??

        String massage = args.GetNext<String>();

        BMSLog.Log($"the unwrapped int was {playerID} \n massage unwrapped is : {massage}");
        Register_Yourself_to_localGM(playerID);
    }
    
    private void Register_Yourself_to_localGM(int playerId)
    {
        //this should be an rpc
        GameLogicManagement_training.Instance.Register_player(
            playerId,this);
        
        BMSLog.Log("I have registered myself to the GM my id : "+ networkObject.MyPlayerId);
    }
    
}
