﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawn : MonoBehaviour
{
    public GameObject itemToSpawn;
    public float repeat_time = 5f;


    void Spawn()
    {
        Instantiate(itemToSpawn,this.transform.position,Quaternion.identity);

    }
    
    
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("Spawn",2f,repeat_time);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
