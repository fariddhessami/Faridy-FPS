﻿using System;
using System.Collections;
using System.Collections.Generic;
using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Generated;
using UnityEngine;
using Vector3Int = UnityEngine.Vector3Int;
using CsharpVoxReader;



public class Chunk : ChunkStuffBehavior
{
    public MeshRenderer MeshRenderer;
    public MeshFilter MeshFilter;    // mesh filter stores the data for the actual mesh
    public MeshCollider MeshCollider;
    


    private static int DimensionSize = 15;
    


    [SerializeField] private VoxelBoolMap _chunkVoxBoolMap;
    
    List<Vector3> vertices = new List<Vector3>(); 
    List<Vector2> uvs = new List<Vector2>();
    List<int> vertsOfTriangles = new List<int>();
    
    
    int vertexIndexOverAll;
    int triangleIndexOverAll;
    List<Vector3Int> xyz_forTriList = new List<Vector3Int>();

    

    void Awake()
    {
        //testing the voxel reader
        FaridyVoxLoader faridyVoxLoader = new FaridyVoxLoader();


        String streamingAssetsPath = Application.streamingAssetsPath;
        String path1 = streamingAssetsPath + "/Art/Vox Files/test 1.vox";
 
        
        VoxReader vReader = new VoxReader(
            path1,
            faridyVoxLoader);

        vReader.Read();
        
        
        
        _chunkVoxBoolMap = faridyVoxLoader.GetVoxelBoolMap();
        
        UpdateGeometery();
    }

    private void UpdateGeometery()
    {
        ArrangeCubesInChunk();

        CreateMesh();
    }

    void Start()
    {
        UpdateGeometery();
    }

    private void ArrangeCubesInChunk()
    {
        vertexIndexOverAll = 0;
        triangleIndexOverAll = 0;

        for (int y = 0; y < _chunkVoxBoolMap.yLength; y++)
        {
            for (int x = 0; x < _chunkVoxBoolMap.xLength; x++)
            {
                for (int z = 0; z < _chunkVoxBoolMap.zLength; z++)
                {
                    if (_chunkVoxBoolMap.BoolMap[x, y, z])
                    {
                        AddCubeToChunk(new Vector3(x, y, z), x, y, z);
                    }
                }
            }
        }
    }
    

    private bool CheckAdjacents(int faceIndex, int x, int y, int z)
    {
        Vector3Int adjIndices = 
            VoxelWorks.AdjacentIndices[faceIndex];

        int xAdj = x + adjIndices.x;
        int yAdj = y + adjIndices.y;
        int zAdj = z + adjIndices.z;
        
        
        if ( xAdj == -1 || yAdj == -1 || zAdj == -1 
             || xAdj == (_chunkVoxBoolMap.xLength) 
             || yAdj == (_chunkVoxBoolMap.yLength) 
             || zAdj == (_chunkVoxBoolMap.zLength) )
        {
            return false;
        }
        
        return _chunkVoxBoolMap.BoolMap[
            xAdj,
            yAdj,
            zAdj
        ];
    }
    
    
    private void AddCubeToChunk(Vector3 position, int x, int y, int z)
    {
        // for each one of 6 vertices that are in a face of the cube
        // we should add the Vector3 of the verts in vertices array
        // another array name triangles will be consisted of
        // index numbers of selected vertices of previous array

        for (int faceIndex = 0; faceIndex < 6; faceIndex++)
        {
            if (!CheckAdjacents(faceIndex,x,y,z))
            {
                for (int i = 0; i < 6; i++)
                {
                    int vertOfFaceIndex = VoxelWorks.VoxelFaces[faceIndex, i];
                    //[face,vertex]
                    vertices.Add(VoxelWorks.VoxelVertices[vertOfFaceIndex] + position);

                    int uvIslandIndex =
                        VoxelWorks.UvIslandsIndexForTriVertex[faceIndex, i];
                    Vector2 uvCoords =
                        VoxelWorks.UvIslandsIndexToVec2A1[uvIslandIndex];

                    uvs.Add(uvCoords);

                    vertsOfTriangles.Add(vertexIndexOverAll);

                    if (i == 2 || i == 5)
                    {
                        xyz_forTriList.Add(new Vector3Int(x, y, z));
                        //implicitly works with vertexIndexOverAll
                        triangleIndexOverAll++;
                    }

                    vertexIndexOverAll++;
                }
            }
        }
    }

    public Vector3Int GetXYZ_byTriIndex(int TriIndex)
    {
        return xyz_forTriList[TriIndex];
    }


    private void Remove_Cube_fromBoolMap(int x, int y, int z)
    {
        if (_chunkVoxBoolMap.BoolMap[x, y, z] == false)
        {
            throw new Exception(
                "wait wait ... the cube wasn't there!!!");
        }
        
        _chunkVoxBoolMap.BoolMap[x, y, z] = false;

        ClearVoxelData();

        UpdateGeometery();
    }

    public void Server_cubeRemove(int x,int y, int z)
    {
        networkObject.SendRpc(
            RPC_RES_REMOVE_CUBE, Receivers.AllBuffered,x,y,z);
    }
    
    public override void ResRemoveCube(RpcArgs args)
    {
        int x = args.GetNext<int>();
        int y = args.GetNext<int>();
        int z = args.GetNext<int>();
        
        Remove_Cube_fromBoolMap(x,y,z);
        
        //do effects here!!
    }
    
    public void ClearVoxelData()
    {
        this.vertices.Clear();
        this.uvs.Clear();
        vertsOfTriangles.Clear();

        xyz_forTriList.Clear();
    }
    
    
    private void CreateMesh()
    {
        Mesh mesh = new Mesh();
        mesh.vertices = vertices.ToArray();
        mesh.triangles = vertsOfTriangles.ToArray();
        mesh.uv = uvs.ToArray();


        mesh.RecalculateNormals();

        MeshFilter.mesh = mesh;
        MeshCollider.sharedMesh = mesh;
    }

    
}
