﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vector3Int = UnityEngine.Vector3Int;

//    this class will only hold data
//    and will not be attached to anyThing

public static class VoxelWorks
{

    //    a static class mostly consisted of look Up tables
    //    for our VoxelWorks

    public static readonly Vector3[] VoxelVertices = new Vector3[8]
    {
        new Vector3(0f, 0f, 0f),
        new Vector3(1f, 0f, 0f),
        new Vector3(1f, 1f, 0f),
        new Vector3(0f, 1f, 0f),
        new Vector3(0f, 0f, 1f),
        new Vector3(1f, 0f, 1f),
        new Vector3(1f, 1f, 1f),
        new Vector3(0f, 1f, 1f)
    };



    public static readonly int[,] VoxelFaces = new int[6, 6]
    {
        // index : [face,vertex]
        {0, 3, 2, 2, 1, 0}, //    front face : 0
        {3, 7, 6, 6, 2, 3}, //    top face : 1
        {5, 6, 7, 7, 4, 5}, //    back face : 2
        {4, 0, 1, 1, 5, 4}, //    bottom face : 3
        {4, 7, 3, 3, 0, 4}, //    left face : 4
        {1, 2, 6, 6, 5, 1} //    right face : 5
    };

    // A1 is the first iteration of mapping
    // UV plane gets to be 4 parts from top Upper Left
    // to lower right quarters are Side,Bottom,Top,NotUsed

    public static readonly Vector2[] UvIslandsIndexToVec2A1 =
        new Vector2[9]
        {
            //    gets index and gives uv coords
            //    guide might be provided to see
            //     how this mapping is done
            new Vector2(0.0f, 0.0f), //0
            new Vector2(0.5f, 0.0f), //1
            new Vector2(1.0f, 0.0f), //2
            new Vector2(0.0f, 0.5f), //3
            new Vector2(0.5f, 0.5f), //4
            new Vector2(1.0f, 0.5f), //5
            new Vector2(0.0f, 1.0f), //6
            new Vector2(0.5f, 1.0f), //7
            new Vector2(1.0f, 1.0f) //8
        };

    public static readonly int[,] UvIslandsIndexForTriVertex = new int[6, 6]
    {
        // indices to be translated to UV coords
        {1, 4, 5, 5, 2, 1}, //texture front - front face : 0
        {0, 3, 4, 4, 1, 0}, //texture top - top face : 1
        {3, 6, 7, 7, 4, 3}, //texture side - back face : 2
        {4, 7, 8, 8, 5, 4}, //texture bottom - bottom face : 3
        {3, 6, 7, 7, 4, 3}, //texture side - left face : 4
        {3, 6, 7, 7, 4, 3} //texture side - right face : 5
    };

    public static readonly Vector3Int[] AdjacentIndices = new Vector3Int[6]
    {
        new Vector3Int(0,0,-1),    //    front Adjacent : 0
        new Vector3Int(0,+1,0),    //    top Adjacent : 1
        new Vector3Int(0,0,+1),    //    back Adjacent : 2
        new Vector3Int(0,-1,0),    //    bottom Adjacent : 3
        new Vector3Int(-1,0,0),    //    left Adjacent : 4
        new Vector3Int(+1,0,0)    //    right Adjacent : 5
    };
    

    public static bool[,,] Generate_Full_xyz_map_old(int xSize, int ySize, int zSize)
    {
        bool[,,] result = new bool[xSize, ySize, zSize];

        for (int y = 0; y < xSize; y++)
        {
            for (int x = 0; x < ySize; x++)
            {
                for (int z = 0; z < zSize; z++)
                {
                    result[x, y, z] = true;
                }
            }
        }

        ;
        return result;
    }
    
    public static VoxelBoolMap Generate_Full_xyz_VoxBoolMap(int xSize, int ySize, int zSize)
    {
        bool[,,] resultBoolMap = new bool[xSize, ySize, zSize];

        for (int y = 0; y < xSize; y++)
        {
            for (int x = 0; x < ySize; x++)
            {
                for (int z = 0; z < zSize; z++)
                {
                    resultBoolMap[x, y, z] = true;
                }
            }
        }
        
        return new VoxelBoolMap(resultBoolMap,xSize,ySize,zSize);
    }
    
}


public struct VoxelBoolMap
{
    
    public bool[,,] BoolMap;
    public int xLength;
    public int yLength;
    public int zLength;
    
    public VoxelBoolMap(bool[,,] boolMap, int xLength, int yLength, int zLength)
    {
        BoolMap = boolMap;
        this.xLength = xLength;
        this.yLength = yLength;
        this.zLength = zLength;
    }
}