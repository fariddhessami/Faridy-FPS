﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CsharpVoxReader;
using System;
using CsharpVoxReader.Chunks;

class FaridyVoxLoader : IVoxLoader
{
    private VoxelBoolMap _voxelBoolMap;
    
    public void LoadModel(Int32 sizeX, Int32 sizeY, Int32 sizeZ, byte[,,] data)
    {
        // Create a model

        
        _voxelBoolMap = MakeVoxelBoolMap(sizeX, sizeY, sizeZ, data);
    }

    private VoxelBoolMap MakeVoxelBoolMap(int sizeX, int sizeY, int sizeZ, byte[,,] data)
    {
        
        
        bool[,,] resultBoolMap = new bool[sizeX,sizeY,sizeZ];
        
        for (int y = 0; y < sizeY; y++)
        {
            for (int x = 0; x < sizeX; x++)
            {
                for (int z = 0; z < sizeZ; z++)
                {
                    if (data[x,y,z] == 0)
                    {
                        // Debug.Log("hey it was zero");
                        resultBoolMap[x,y,z] = false;
                    }else
                    {
                        // Debug.Log("hey it wasn't zero");
                        resultBoolMap[x,y,z] = true;
                    }
                }
            }
        }
        
        return new VoxelBoolMap(resultBoolMap,sizeX,sizeY,sizeZ);
    }

    public void LoadPalette(UInt32[] palette)
    {
        // Set palette

        // You can use extension method ToARGB from namespace CsharpVoxReader
        // To get rgba values
        // ie: palette[1].ToARGB(out a, out r, out g, out b)


        Debug.Log($"palette.Length : {palette.Length}");
        
        for (int i = 0; i < palette.Length; i++)
        {
            palette[i] = UInt32.MaxValue;
        }
        
    }

    public VoxelBoolMap GetVoxelBoolMap()
    {
        return _voxelBoolMap;
    }
    
    public void SetModelCount(int count)
    {
        // throw new NotImplementedException();
    }

    public void SetMaterialOld(int paletteId, MaterialOld.MaterialTypes type, float weight, MaterialOld.PropertyBits property, float normalized)
    {
        // throw new NotImplementedException();
    }

    public void NewTransformNode(int id, int childNodeId, int layerId, Dictionary<string, byte[]>[] framesAttributes)
    {
        // throw new NotImplementedException();
    }

    public void NewGroupNode(int id, Dictionary<string, byte[]> attributes, int[] childrenIds)
    {
        // throw new NotImplementedException();
    }

    public void NewShapeNode(int id, Dictionary<string, byte[]> attributes, int[] modelIds, Dictionary<string, byte[]>[] modelsAttributes)
    {
        // throw new NotImplementedException();
    }

    public void NewMaterial(int id, Dictionary<string, byte[]> attributes)
    {
        // throw new NotImplementedException();
    }

    public void NewLayer(int id, Dictionary<string, byte[]> attributes)
    {
        // throw new NotImplementedException();
    }
}
