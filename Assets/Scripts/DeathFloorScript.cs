﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DeathFloorScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    private void OnCollisionEnter(Collision collision)
    {
        // Debug.Log("coll* death floor : " + collision.gameObject.transform.name 
        //                                  + " tag : " + collision.gameObject.transform.tag);
        
        GameObject colidingGameObject = collision.gameObject;
        KillCollidingPlayer(colidingGameObject);
    }

    private static void KillCollidingPlayer(GameObject collidingGameObject)
    {
        
        if (collidingGameObject.CompareTag("PlayerCubeBottom"))
        {
            PlayerHealth departedPlayerHealth = collidingGameObject.transform.GetComponentInParent<PlayerHealth>();

            if (departedPlayerHealth != null)
            {
                if (departedPlayerHealth.networkObject.IsServer)
                {
                    Debug.Log("death floor wants to kill player");

                    if (departedPlayerHealth.Life_status() == Life_Status_enum.alive)
                        // departedPlayerHealth.MakeDieReqByServer(); //only server can make this request
                        departedPlayerHealth.MakeReqChangeHp
                            (-PlayerHealth.max_Hp,-1); //only server can make this request   
                }
                else
                {
                    Debug.Log("death floor CAN'T kill player");
                }
            }
        }
    }

    private void OnTriggerEnter(Collider collidingTHing)
    {
        
    }
    
}
