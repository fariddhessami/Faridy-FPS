﻿using System.Collections;
using System.Collections.Generic;
using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Generated;
using UnityEngine;

public class A_Class : AClassStuffBehavior
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.M))
        {
            ShootFunc();
        }
        
        // networkObject.SendRpc(RPC_REQ_SHOOT, Receivers.Server,
        //     startPos,
        //     direction);
    }

    public override void AClassFunc(RpcArgs args)
    {
        throw new System.NotImplementedException();
    }


    protected virtual void ShootFunc()
    {
        Debug.Log("SHOOT A");
        
        return;
    }

}
