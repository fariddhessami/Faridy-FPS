﻿using System;
using System.Collections;
using System.Collections.Generic;
using BeardedManStudios.Forge.Logging;
using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Generated;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMove : PlayerMoveStuffBehavior
{

    [SerializeField] private string move_H_input, move_V_input;
    //note : these are names for referencing the Unity input
    
    [SerializeField] private string jump_input = "Jump";

    private bool _weHaveAjump = false;

    private bool isJumping = false;
    private bool isFalling = false;

    private bool _isPlayerGrounded;
    
    //    because the CharacterController.isGrounded is whether the
    //    character was touching the ground in the last move or not
    //    so we should  keep a value for being grounded
    
    [SerializeField] private float xz_SpeedCoefficient = 0.6f;

    private CharacterController _charController;
    
    private PlayerHealth _playerHealth;

    private PlayerLook _playerLook;

    private PlayerProperties _playerProperties;
    
    private Vector3 move_xz_Speed;
    private Vector3 move_y_Speed;
    
    
    private Vector3 _current_xz_Speed;
    private Vector3 _current_y_Speed;


    private Text ui_text_test;
    
    
    [SerializeField] private float g_Coefficient = 2f;//Gravitational acceleration
    [SerializeField] private float jump_Coefficient = 30f;
    
    [SerializeField] private float _xz_acceleration_coe = 5f;

    [SerializeField] private Animator _playerAnimator;
    [SerializeField] private Animator _fpsWeaponAnimator;

    
    private float _updatePosTime = 1.0f;
    private float _nextUpdatePosTimer = 0.0f;
    
    void Start()
    {
        _current_xz_Speed = new Vector3(0f,0f,0f);
        _current_y_Speed = new Vector3(0f,0f,0f);
    }

    void Awake()
    {
        _charController = GetComponent<CharacterController>();
        _playerHealth = GetComponent<PlayerHealth>();
        _playerLook = GetComponentInChildren<PlayerLook>();
        _playerProperties = GetComponent<PlayerProperties>();

        ui_text_test = GetComponentInChildren<Text>();
    }


    protected override void NetworkStart()
    {
        base.NetworkStart();
        
        
        if (networkObject.IsOwner)
        {

            bool isTHisMyPlayer = networkObject.IsOwner;
            _playerProperties.FetchNameFromSettingManager(isTHisMyPlayer);
            
            
            //    we're gonna tell everybody else what our name is :
            //    by the way we're gonna get ID from the server
            //    also the other clone would register themselves
            
            SubmitNameAndGetIdFromServer(_playerProperties.GetName());
        }
        else
        {
            //    turning off display for 1stPerson WeaponHolder
            
            _playerProperties.GetFirstPerWh().Turn_display_off();
            _playerProperties.GetThirdPerWh().Turn_display_on();
        }
    }//NetworkStart

    // private void MakeRegisterToGmRpc()
    // {
    //     networkObject.SendRpc
    //         (RPC_RES_REGITER_TO_GM_LIST, Receivers.AllBuffered,10);
    // }

    void Update()
    {
        if (networkObject == null)
        {
            //waiting for networkObject on behalf of PlayerMove
            return;
        }


        
        
        
        if (!networkObject.IsOwner)
        {
            float timeT = Time.time;
        

            if (timeT.CompareTo(_nextUpdatePosTimer)==1)
            {
                // Debug.Log("update pos time");
                _nextUpdatePosTimer = timeT + _updatePosTime;
                transform.position = networkObject.playerPosition;
                
            }
            else
            {
                _charController.Move(networkObject.playerSpeedVec * Time.deltaTime);
            }
            
            HandleRunAnim(networkObject.playerSpeedVec);
        }
        else
        {
            //    Player is owned    !!!
            
            PlayerMovement();
            networkObject.playerPosition = transform.position;
        }

    }



    private void PlayerMovement()
    {

        if (_playerHealth.Life_status() != Life_Status_enum.alive)
        {
            //lock yourself !!
            return;
        }

        float hMove = Input.GetAxis(move_H_input)  ;
        float vMove = Input.GetAxis(move_V_input)  ;

        
        bool jButtenPressed = Input.GetButtonDown(jump_input);
        if (jButtenPressed)
        {
            _weHaveAjump = true;
            // charController doesn't understand in every frame that
            // character might be on the ground
        }

        Vector3 local_forwBack_motVec = this.transform.forward * vMove;
        Vector3 local_leftRight_motVec = this.transform.right * hMove;
        Vector3 wishDirection =
            (local_forwBack_motVec + local_leftRight_motVec).normalized;

        //the old one :
        // move_xz_Speed = wishDirection * movementSpeedCoefficient;

        //the new one :
        move_xz_Speed = _current_xz_Speed + wishDirection * _xz_acceleration_coe;

        Player_Jump_Fall();
        
        
        

        Vector3 move_Speed = (move_xz_Speed * xz_SpeedCoefficient) + move_y_Speed;

        _charController.Move( move_Speed * Time.deltaTime);
        
        networkObject.playerSpeedVec = move_Speed;

        HandleRunAnim(move_Speed);
        
    }

    private void HandleRunAnim(Vector3 move_Speed)
    {
        if (move_Speed.magnitude > 2f)
        {
            _playerAnimator.SetBool("isRunning", true);
            _fpsWeaponAnimator.SetBool("isRunning", true);
            // Debug.Log("isRunning : "+true);;
        }
        else
        {
            _playerAnimator.SetBool("isRunning", false);
            _fpsWeaponAnimator.SetBool("isRunning", false);
        }
    }
    //PlayerMovement

    private Vector3 Player_Jump_Fall()
    {
        if (_charController.isGrounded)
        {
            // // Debug.Log("Grounded !");
            // if (_charController.collisionFlags == CollisionFlags.CollidedBelow)
            // {
            //     _charController.GetComponent<Collider>().
            // }
        }
        
        if (!_charController.isGrounded)
        {
            // if(_weHaveAjump)
            //     Debug.Log("jump started");
            
            move_y_Speed = move_y_Speed + (g_Coefficient * Vector3.down)* Time.deltaTime;
        }
        else
        {
            // isJumping = false;
    
            if (_weHaveAjump)
            {
                move_y_Speed = move_y_Speed + Vector3.up * jump_Coefficient;
                _weHaveAjump = false;
            }
            else
            {
                move_y_Speed = new Vector3(0, 0, 0);
            }
        }
    
        return move_y_Speed;
    }


    // Update is called once per frame
  


    public void Spawn_player_transform(Vector3 spwan_point_pos,Quaternion spwan_point_rot)
    {
        _charController.enabled = false;
        // Debug.Log("charCntller enabled for repos");
        transform.position = spwan_point_pos;
        transform.rotation = spwan_point_rot;
        // Debug.Log("charCntller enabled");
        _charController.enabled = true;
    }
    
    
    
    private void SubmitNameAndGetIdFromServer(String name)
    {
        MakeRequestForRegisterAndId(name);
    }



    public override void ResRewardSelf(RpcArgs args)
    {
        GameManagement.Instance.RewardPlayer(this);
    }

    public void MakeRequestForRegisterAndId(String name)
    {
        //    this is owner requesting to get registered
        
        if (networkObject.IsOwner)
        {
            networkObject.SendRpc(
                RPC_REQ_REGITER_TO_GM_LIST, Receivers.Server,name);
        }
        else
        {
            throw new Exception("only owner should do this request");
        }
    }
    
    public override void ReqRegiterToGmList(RpcArgs args)
    {
        //    this is server's responding to the request

        // Debug.Log("hello hello 1234");
        
        if (networkObject.IsServer)
        {
            //    we're gonna get an ID from server
            int playerId = GameManagement.Instance.GeneratePlayerId();
            
            //    we've got the name from the guy who made the request
            String name = args.GetNext<String>();
            
            //    now telling everyone's GM to add this guy
            networkObject.SendRpc(
                RPC_RES_REGITER_TO_GM_LIST,
                Receivers.AllBuffered,playerId,name);
        }
        else
        {
            throw new Exception("only server should process this request");
        }
    }
    
    public override void ResRegiterToGmList(RpcArgs args)
    {
        //    so idea is each individual player registers itself to
        //    the GM, but they get ID and name from server

        int id = args.GetNext<int>();
        String name = args.GetNext<String>();

        _playerProperties.PlayerId = id;
        _playerProperties.SetNameAndNameTag(name);
        
        GameManagement.Instance.AddPlayerToTheList(this,id);
    }

    public String GetPlayerName()
    {
        return  _playerProperties.GetName();
    }

    public void ProcessRewardSelf()
    {
        networkObject.SendRpc(
            RPC_RES_REWARD_SELF, Receivers.AllBuffered);
    }

    public int GetPlayerId()
    {
        return _playerProperties.PlayerId;
    }
}
