using BeardedManStudios.Forge.Networking.Frame;
using BeardedManStudios.Forge.Networking.Unity;
using System;
using UnityEngine;

namespace BeardedManStudios.Forge.Networking.Generated
{
	[GeneratedInterpol("{\"inter\":[50,50]")]
	public partial class PlayerMoveStuffNetworkObject : NetworkObject
	{
		public const int IDENTITY = 21;

		private byte[] _dirtyFields = new byte[1];

		#pragma warning disable 0067
		public event FieldChangedEvent fieldAltered;
		#pragma warning restore 0067
		[ForgeGeneratedField]
		private Vector3 _playerPosition;
		public event FieldEvent<Vector3> playerPositionChanged;
		public InterpolateVector3 playerPositionInterpolation = new InterpolateVector3() { LerpT = 50f, Enabled = true };
		public Vector3 playerPosition
		{
			get { return _playerPosition; }
			set
			{
				// Don't do anything if the value is the same
				if (_playerPosition == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[0] |= 0x1;
				_playerPosition = value;
				hasDirtyFields = true;
			}
		}

		public void SetplayerPositionDirty()
		{
			_dirtyFields[0] |= 0x1;
			hasDirtyFields = true;
		}

		private void RunChange_playerPosition(ulong timestep)
		{
			if (playerPositionChanged != null) playerPositionChanged(_playerPosition, timestep);
			if (fieldAltered != null) fieldAltered("playerPosition", _playerPosition, timestep);
		}
		[ForgeGeneratedField]
		private Vector3 _playerSpeedVec;
		public event FieldEvent<Vector3> playerSpeedVecChanged;
		public InterpolateVector3 playerSpeedVecInterpolation = new InterpolateVector3() { LerpT = 50f, Enabled = true };
		public Vector3 playerSpeedVec
		{
			get { return _playerSpeedVec; }
			set
			{
				// Don't do anything if the value is the same
				if (_playerSpeedVec == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[0] |= 0x2;
				_playerSpeedVec = value;
				hasDirtyFields = true;
			}
		}

		public void SetplayerSpeedVecDirty()
		{
			_dirtyFields[0] |= 0x2;
			hasDirtyFields = true;
		}

		private void RunChange_playerSpeedVec(ulong timestep)
		{
			if (playerSpeedVecChanged != null) playerSpeedVecChanged(_playerSpeedVec, timestep);
			if (fieldAltered != null) fieldAltered("playerSpeedVec", _playerSpeedVec, timestep);
		}

		protected override void OwnershipChanged()
		{
			base.OwnershipChanged();
			SnapInterpolations();
		}
		
		public void SnapInterpolations()
		{
			playerPositionInterpolation.current = playerPositionInterpolation.target;
			playerSpeedVecInterpolation.current = playerSpeedVecInterpolation.target;
		}

		public override int UniqueIdentity { get { return IDENTITY; } }

		protected override BMSByte WritePayload(BMSByte data)
		{
			UnityObjectMapper.Instance.MapBytes(data, _playerPosition);
			UnityObjectMapper.Instance.MapBytes(data, _playerSpeedVec);

			return data;
		}

		protected override void ReadPayload(BMSByte payload, ulong timestep)
		{
			_playerPosition = UnityObjectMapper.Instance.Map<Vector3>(payload);
			playerPositionInterpolation.current = _playerPosition;
			playerPositionInterpolation.target = _playerPosition;
			RunChange_playerPosition(timestep);
			_playerSpeedVec = UnityObjectMapper.Instance.Map<Vector3>(payload);
			playerSpeedVecInterpolation.current = _playerSpeedVec;
			playerSpeedVecInterpolation.target = _playerSpeedVec;
			RunChange_playerSpeedVec(timestep);
		}

		protected override BMSByte SerializeDirtyFields()
		{
			dirtyFieldsData.Clear();
			dirtyFieldsData.Append(_dirtyFields);

			if ((0x1 & _dirtyFields[0]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _playerPosition);
			if ((0x2 & _dirtyFields[0]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _playerSpeedVec);

			// Reset all the dirty fields
			for (int i = 0; i < _dirtyFields.Length; i++)
				_dirtyFields[i] = 0;

			return dirtyFieldsData;
		}

		protected override void ReadDirtyFields(BMSByte data, ulong timestep)
		{
			if (readDirtyFlags == null)
				Initialize();

			Buffer.BlockCopy(data.byteArr, data.StartIndex(), readDirtyFlags, 0, readDirtyFlags.Length);
			data.MoveStartIndex(readDirtyFlags.Length);

			if ((0x1 & readDirtyFlags[0]) != 0)
			{
				if (playerPositionInterpolation.Enabled)
				{
					playerPositionInterpolation.target = UnityObjectMapper.Instance.Map<Vector3>(data);
					playerPositionInterpolation.Timestep = timestep;
				}
				else
				{
					_playerPosition = UnityObjectMapper.Instance.Map<Vector3>(data);
					RunChange_playerPosition(timestep);
				}
			}
			if ((0x2 & readDirtyFlags[0]) != 0)
			{
				if (playerSpeedVecInterpolation.Enabled)
				{
					playerSpeedVecInterpolation.target = UnityObjectMapper.Instance.Map<Vector3>(data);
					playerSpeedVecInterpolation.Timestep = timestep;
				}
				else
				{
					_playerSpeedVec = UnityObjectMapper.Instance.Map<Vector3>(data);
					RunChange_playerSpeedVec(timestep);
				}
			}
		}

		public override void InterpolateUpdate()
		{
			if (IsOwner)
				return;

			if (playerPositionInterpolation.Enabled && !playerPositionInterpolation.current.UnityNear(playerPositionInterpolation.target, 0.0015f))
			{
				_playerPosition = (Vector3)playerPositionInterpolation.Interpolate();
				//RunChange_playerPosition(playerPositionInterpolation.Timestep);
			}
			if (playerSpeedVecInterpolation.Enabled && !playerSpeedVecInterpolation.current.UnityNear(playerSpeedVecInterpolation.target, 0.0015f))
			{
				_playerSpeedVec = (Vector3)playerSpeedVecInterpolation.Interpolate();
				//RunChange_playerSpeedVec(playerSpeedVecInterpolation.Timestep);
			}
		}

		private void Initialize()
		{
			if (readDirtyFlags == null)
				readDirtyFlags = new byte[1];

		}

		public PlayerMoveStuffNetworkObject() : base() { Initialize(); }
		public PlayerMoveStuffNetworkObject(NetWorker networker, INetworkBehavior networkBehavior = null, int createCode = 0, byte[] metadata = null) : base(networker, networkBehavior, createCode, metadata) { Initialize(); }
		public PlayerMoveStuffNetworkObject(NetWorker networker, uint serverId, FrameStream frame) : base(networker, serverId, frame) { Initialize(); }

		// DO NOT TOUCH, THIS GETS GENERATED PLEASE EXTEND THIS CLASS IF YOU WISH TO HAVE CUSTOM CODE ADDITIONS
	}
}
