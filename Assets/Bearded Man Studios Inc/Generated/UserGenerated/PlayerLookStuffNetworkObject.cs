using BeardedManStudios.Forge.Networking.Frame;
using BeardedManStudios.Forge.Networking.Unity;
using System;
using UnityEngine;

namespace BeardedManStudios.Forge.Networking.Generated
{
	[GeneratedInterpol("{\"inter\":[10,10]")]
	public partial class PlayerLookStuffNetworkObject : NetworkObject
	{
		public const int IDENTITY = 15;

		private byte[] _dirtyFields = new byte[1];

		#pragma warning disable 0067
		public event FieldChangedEvent fieldAltered;
		#pragma warning restore 0067
		[ForgeGeneratedField]
		private Quaternion _playerBodyDirection;
		public event FieldEvent<Quaternion> playerBodyDirectionChanged;
		public InterpolateQuaternion playerBodyDirectionInterpolation = new InterpolateQuaternion() { LerpT = 10f, Enabled = true };
		public Quaternion playerBodyDirection
		{
			get { return _playerBodyDirection; }
			set
			{
				// Don't do anything if the value is the same
				if (_playerBodyDirection == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[0] |= 0x1;
				_playerBodyDirection = value;
				hasDirtyFields = true;
			}
		}

		public void SetplayerBodyDirectionDirty()
		{
			_dirtyFields[0] |= 0x1;
			hasDirtyFields = true;
		}

		private void RunChange_playerBodyDirection(ulong timestep)
		{
			if (playerBodyDirectionChanged != null) playerBodyDirectionChanged(_playerBodyDirection, timestep);
			if (fieldAltered != null) fieldAltered("playerBodyDirection", _playerBodyDirection, timestep);
		}
		[ForgeGeneratedField]
		private Quaternion _playerCameraDirection;
		public event FieldEvent<Quaternion> playerCameraDirectionChanged;
		public InterpolateQuaternion playerCameraDirectionInterpolation = new InterpolateQuaternion() { LerpT = 10f, Enabled = true };
		public Quaternion playerCameraDirection
		{
			get { return _playerCameraDirection; }
			set
			{
				// Don't do anything if the value is the same
				if (_playerCameraDirection == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[0] |= 0x2;
				_playerCameraDirection = value;
				hasDirtyFields = true;
			}
		}

		public void SetplayerCameraDirectionDirty()
		{
			_dirtyFields[0] |= 0x2;
			hasDirtyFields = true;
		}

		private void RunChange_playerCameraDirection(ulong timestep)
		{
			if (playerCameraDirectionChanged != null) playerCameraDirectionChanged(_playerCameraDirection, timestep);
			if (fieldAltered != null) fieldAltered("playerCameraDirection", _playerCameraDirection, timestep);
		}

		protected override void OwnershipChanged()
		{
			base.OwnershipChanged();
			SnapInterpolations();
		}
		
		public void SnapInterpolations()
		{
			playerBodyDirectionInterpolation.current = playerBodyDirectionInterpolation.target;
			playerCameraDirectionInterpolation.current = playerCameraDirectionInterpolation.target;
		}

		public override int UniqueIdentity { get { return IDENTITY; } }

		protected override BMSByte WritePayload(BMSByte data)
		{
			UnityObjectMapper.Instance.MapBytes(data, _playerBodyDirection);
			UnityObjectMapper.Instance.MapBytes(data, _playerCameraDirection);

			return data;
		}

		protected override void ReadPayload(BMSByte payload, ulong timestep)
		{
			_playerBodyDirection = UnityObjectMapper.Instance.Map<Quaternion>(payload);
			playerBodyDirectionInterpolation.current = _playerBodyDirection;
			playerBodyDirectionInterpolation.target = _playerBodyDirection;
			RunChange_playerBodyDirection(timestep);
			_playerCameraDirection = UnityObjectMapper.Instance.Map<Quaternion>(payload);
			playerCameraDirectionInterpolation.current = _playerCameraDirection;
			playerCameraDirectionInterpolation.target = _playerCameraDirection;
			RunChange_playerCameraDirection(timestep);
		}

		protected override BMSByte SerializeDirtyFields()
		{
			dirtyFieldsData.Clear();
			dirtyFieldsData.Append(_dirtyFields);

			if ((0x1 & _dirtyFields[0]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _playerBodyDirection);
			if ((0x2 & _dirtyFields[0]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _playerCameraDirection);

			// Reset all the dirty fields
			for (int i = 0; i < _dirtyFields.Length; i++)
				_dirtyFields[i] = 0;

			return dirtyFieldsData;
		}

		protected override void ReadDirtyFields(BMSByte data, ulong timestep)
		{
			if (readDirtyFlags == null)
				Initialize();

			Buffer.BlockCopy(data.byteArr, data.StartIndex(), readDirtyFlags, 0, readDirtyFlags.Length);
			data.MoveStartIndex(readDirtyFlags.Length);

			if ((0x1 & readDirtyFlags[0]) != 0)
			{
				if (playerBodyDirectionInterpolation.Enabled)
				{
					playerBodyDirectionInterpolation.target = UnityObjectMapper.Instance.Map<Quaternion>(data);
					playerBodyDirectionInterpolation.Timestep = timestep;
				}
				else
				{
					_playerBodyDirection = UnityObjectMapper.Instance.Map<Quaternion>(data);
					RunChange_playerBodyDirection(timestep);
				}
			}
			if ((0x2 & readDirtyFlags[0]) != 0)
			{
				if (playerCameraDirectionInterpolation.Enabled)
				{
					playerCameraDirectionInterpolation.target = UnityObjectMapper.Instance.Map<Quaternion>(data);
					playerCameraDirectionInterpolation.Timestep = timestep;
				}
				else
				{
					_playerCameraDirection = UnityObjectMapper.Instance.Map<Quaternion>(data);
					RunChange_playerCameraDirection(timestep);
				}
			}
		}

		public override void InterpolateUpdate()
		{
			if (IsOwner)
				return;

			if (playerBodyDirectionInterpolation.Enabled && !playerBodyDirectionInterpolation.current.UnityNear(playerBodyDirectionInterpolation.target, 0.0015f))
			{
				_playerBodyDirection = (Quaternion)playerBodyDirectionInterpolation.Interpolate();
				//RunChange_playerBodyDirection(playerBodyDirectionInterpolation.Timestep);
			}
			if (playerCameraDirectionInterpolation.Enabled && !playerCameraDirectionInterpolation.current.UnityNear(playerCameraDirectionInterpolation.target, 0.0015f))
			{
				_playerCameraDirection = (Quaternion)playerCameraDirectionInterpolation.Interpolate();
				//RunChange_playerCameraDirection(playerCameraDirectionInterpolation.Timestep);
			}
		}

		private void Initialize()
		{
			if (readDirtyFlags == null)
				readDirtyFlags = new byte[1];

		}

		public PlayerLookStuffNetworkObject() : base() { Initialize(); }
		public PlayerLookStuffNetworkObject(NetWorker networker, INetworkBehavior networkBehavior = null, int createCode = 0, byte[] metadata = null) : base(networker, networkBehavior, createCode, metadata) { Initialize(); }
		public PlayerLookStuffNetworkObject(NetWorker networker, uint serverId, FrameStream frame) : base(networker, serverId, frame) { Initialize(); }

		// DO NOT TOUCH, THIS GETS GENERATED PLEASE EXTEND THIS CLASS IF YOU WISH TO HAVE CUSTOM CODE ADDITIONS
	}
}
