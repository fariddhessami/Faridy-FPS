using BeardedManStudios.Forge.Networking.Frame;
using BeardedManStudios.Forge.Networking.Unity;
using System;
using UnityEngine;

namespace BeardedManStudios.Forge.Networking.Generated
{
	[GeneratedInterpol("{\"inter\":[0]")]
	public partial class PlayerPropertiesStuffNetworkObject : NetworkObject
	{
		public const int IDENTITY = 17;

		private byte[] _dirtyFields = new byte[1];

		#pragma warning disable 0067
		public event FieldChangedEvent fieldAltered;
		#pragma warning restore 0067
		[ForgeGeneratedField]
		private int _idtoShare;
		public event FieldEvent<int> idtoShareChanged;
		public Interpolated<int> idtoShareInterpolation = new Interpolated<int>() { LerpT = 0f, Enabled = false };
		public int idtoShare
		{
			get { return _idtoShare; }
			set
			{
				// Don't do anything if the value is the same
				if (_idtoShare == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[0] |= 0x1;
				_idtoShare = value;
				hasDirtyFields = true;
			}
		}

		public void SetidtoShareDirty()
		{
			_dirtyFields[0] |= 0x1;
			hasDirtyFields = true;
		}

		private void RunChange_idtoShare(ulong timestep)
		{
			if (idtoShareChanged != null) idtoShareChanged(_idtoShare, timestep);
			if (fieldAltered != null) fieldAltered("idtoShare", _idtoShare, timestep);
		}

		protected override void OwnershipChanged()
		{
			base.OwnershipChanged();
			SnapInterpolations();
		}
		
		public void SnapInterpolations()
		{
			idtoShareInterpolation.current = idtoShareInterpolation.target;
		}

		public override int UniqueIdentity { get { return IDENTITY; } }

		protected override BMSByte WritePayload(BMSByte data)
		{
			UnityObjectMapper.Instance.MapBytes(data, _idtoShare);

			return data;
		}

		protected override void ReadPayload(BMSByte payload, ulong timestep)
		{
			_idtoShare = UnityObjectMapper.Instance.Map<int>(payload);
			idtoShareInterpolation.current = _idtoShare;
			idtoShareInterpolation.target = _idtoShare;
			RunChange_idtoShare(timestep);
		}

		protected override BMSByte SerializeDirtyFields()
		{
			dirtyFieldsData.Clear();
			dirtyFieldsData.Append(_dirtyFields);

			if ((0x1 & _dirtyFields[0]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _idtoShare);

			// Reset all the dirty fields
			for (int i = 0; i < _dirtyFields.Length; i++)
				_dirtyFields[i] = 0;

			return dirtyFieldsData;
		}

		protected override void ReadDirtyFields(BMSByte data, ulong timestep)
		{
			if (readDirtyFlags == null)
				Initialize();

			Buffer.BlockCopy(data.byteArr, data.StartIndex(), readDirtyFlags, 0, readDirtyFlags.Length);
			data.MoveStartIndex(readDirtyFlags.Length);

			if ((0x1 & readDirtyFlags[0]) != 0)
			{
				if (idtoShareInterpolation.Enabled)
				{
					idtoShareInterpolation.target = UnityObjectMapper.Instance.Map<int>(data);
					idtoShareInterpolation.Timestep = timestep;
				}
				else
				{
					_idtoShare = UnityObjectMapper.Instance.Map<int>(data);
					RunChange_idtoShare(timestep);
				}
			}
		}

		public override void InterpolateUpdate()
		{
			if (IsOwner)
				return;

			if (idtoShareInterpolation.Enabled && !idtoShareInterpolation.current.UnityNear(idtoShareInterpolation.target, 0.0015f))
			{
				_idtoShare = (int)idtoShareInterpolation.Interpolate();
				//RunChange_idtoShare(idtoShareInterpolation.Timestep);
			}
		}

		private void Initialize()
		{
			if (readDirtyFlags == null)
				readDirtyFlags = new byte[1];

		}

		public PlayerPropertiesStuffNetworkObject() : base() { Initialize(); }
		public PlayerPropertiesStuffNetworkObject(NetWorker networker, INetworkBehavior networkBehavior = null, int createCode = 0, byte[] metadata = null) : base(networker, networkBehavior, createCode, metadata) { Initialize(); }
		public PlayerPropertiesStuffNetworkObject(NetWorker networker, uint serverId, FrameStream frame) : base(networker, serverId, frame) { Initialize(); }

		// DO NOT TOUCH, THIS GETS GENERATED PLEASE EXTEND THIS CLASS IF YOU WISH TO HAVE CUSTOM CODE ADDITIONS
	}
}
