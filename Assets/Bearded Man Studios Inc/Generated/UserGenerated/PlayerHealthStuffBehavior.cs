using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Unity;
using UnityEngine;

namespace BeardedManStudios.Forge.Networking.Generated
{
	[GeneratedRPC("{\"types\":[[\"float\", \"int\"][][\"float\", \"int\"][][\"Vector3\", \"Quaternion\"]]")]
	[GeneratedRPCVariableNames("{\"types\":[[\"hpChangeAmount\", \"playerCauser\"][][\"hpChangeAmount\", \"causerId\"][][\"resPos\", \"resRot\"]]")]
	public abstract partial class PlayerHealthStuffBehavior : NetworkBehavior
	{
		public const byte RPC_REQ_CHANGE_HP = 0 + 5;
		public const byte RPC_DIE = 1 + 5;
		public const byte RPC_RES_CHANGE_HP = 2 + 5;
		public const byte RPC_REQ_RESPAWN = 3 + 5;
		public const byte RPC_LOCAL_RESPAWN = 4 + 5;
		
		public PlayerHealthStuffNetworkObject networkObject = null;

		public override void Initialize(NetworkObject obj)
		{
			// We have already initialized this object
			if (networkObject != null && networkObject.AttachedBehavior != null)
				return;
			
			networkObject = (PlayerHealthStuffNetworkObject)obj;
			networkObject.AttachedBehavior = this;

			base.SetupHelperRpcs(networkObject);
			networkObject.RegisterRpc("ReqChangeHp", ReqChangeHp, typeof(float), typeof(int));
			networkObject.RegisterRpc("Die", Die);
			networkObject.RegisterRpc("ResChangeHp", ResChangeHp, typeof(float), typeof(int));
			networkObject.RegisterRpc("ReqRespawn", ReqRespawn);
			networkObject.RegisterRpc("LocalRespawn", LocalRespawn, typeof(Vector3), typeof(Quaternion));

			networkObject.onDestroy += DestroyGameObject;

			if (!obj.IsOwner)
			{
				if (!skipAttachIds.ContainsKey(obj.NetworkId)){
					uint newId = obj.NetworkId + 1;
					ProcessOthers(gameObject.transform, ref newId);
				}
				else
					skipAttachIds.Remove(obj.NetworkId);
			}

			if (obj.Metadata != null)
			{
				byte transformFlags = obj.Metadata[0];

				if (transformFlags != 0)
				{
					BMSByte metadataTransform = new BMSByte();
					metadataTransform.Clone(obj.Metadata);
					metadataTransform.MoveStartIndex(1);

					if ((transformFlags & 0x01) != 0 && (transformFlags & 0x02) != 0)
					{
						MainThreadManager.Run(() =>
						{
							transform.position = ObjectMapper.Instance.Map<Vector3>(metadataTransform);
							transform.rotation = ObjectMapper.Instance.Map<Quaternion>(metadataTransform);
						});
					}
					else if ((transformFlags & 0x01) != 0)
					{
						MainThreadManager.Run(() => { transform.position = ObjectMapper.Instance.Map<Vector3>(metadataTransform); });
					}
					else if ((transformFlags & 0x02) != 0)
					{
						MainThreadManager.Run(() => { transform.rotation = ObjectMapper.Instance.Map<Quaternion>(metadataTransform); });
					}
				}
			}

			MainThreadManager.Run(() =>
			{
				NetworkStart();
				networkObject.Networker.FlushCreateActions(networkObject);
			});
		}

		protected override void CompleteRegistration()
		{
			base.CompleteRegistration();
			networkObject.ReleaseCreateBuffer();
		}

		public override void Initialize(NetWorker networker, byte[] metadata = null)
		{
			Initialize(new PlayerHealthStuffNetworkObject(networker, createCode: TempAttachCode, metadata: metadata));
		}

		private void DestroyGameObject(NetWorker sender)
		{
			MainThreadManager.Run(() => { try { Destroy(gameObject); } catch { } });
			networkObject.onDestroy -= DestroyGameObject;
		}

		public override NetworkObject CreateNetworkObject(NetWorker networker, int createCode, byte[] metadata = null)
		{
			return new PlayerHealthStuffNetworkObject(networker, this, createCode, metadata);
		}

		protected override void InitializedTransform()
		{
			networkObject.SnapInterpolations();
		}

		/// <summary>
		/// Arguments:
		/// float hpChangeAmount
		/// int playerCauser
		/// </summary>
		public abstract void ReqChangeHp(RpcArgs args);
		/// <summary>
		/// Arguments:
		/// </summary>
		public abstract void Die(RpcArgs args);
		/// <summary>
		/// Arguments:
		/// </summary>
		public abstract void ResChangeHp(RpcArgs args);
		/// <summary>
		/// Arguments:
		/// </summary>
		public abstract void ReqRespawn(RpcArgs args);
		/// <summary>
		/// Arguments:
		/// </summary>
		public abstract void LocalRespawn(RpcArgs args);

		// DO NOT TOUCH, THIS GETS GENERATED PLEASE EXTEND THIS CLASS IF YOU WISH TO HAVE CUSTOM CODE ADDITIONS
	}
}