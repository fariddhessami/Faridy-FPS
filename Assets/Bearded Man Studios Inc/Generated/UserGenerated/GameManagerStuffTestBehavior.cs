using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Unity;
using UnityEngine;

namespace BeardedManStudios.Forge.Networking.Generated
{
	[GeneratedRPC("{\"types\":[[\"int\"][\"int\", \"Vector3\", \"Quaternion\"][\"int\"][][\"int\", \"float\"][\"int\", \"int\"]]")]
	[GeneratedRPCVariableNames("{\"types\":[[\"requestedPlayerID\"][\"playerID\", \"spawnPointPos\", \"spawnPointRot\"][\"roundSeconds\"][][\"gameRoundState\", \"secondsRemaining\"][\"playerId\", \"rewardValue\"]]")]
	public abstract partial class GameManagerStuffTestBehavior : NetworkBehavior
	{
		public const byte RPC_REQUEST_PLAYER_RESPAWN = 0 + 5;
		public const byte RPC_LOCAL_RE_SPAWN_SERVICE = 1 + 5;
		public const byte RPC_UPDATE_ROUND_TIME = 2 + 5;
		public const byte RPC_REQ_SYNCH_GAME_INFO = 3 + 5;
		public const byte RPC_RES_SYNCH_GAME_INFO = 4 + 5;
		public const byte RPC_RES_REWARD_PLAYER_BY_ID = 5 + 5;
		
		public GameManagerStuffTestNetworkObject networkObject = null;

		public override void Initialize(NetworkObject obj)
		{
			// We have already initialized this object
			if (networkObject != null && networkObject.AttachedBehavior != null)
				return;
			
			networkObject = (GameManagerStuffTestNetworkObject)obj;
			networkObject.AttachedBehavior = this;

			base.SetupHelperRpcs(networkObject);
			networkObject.RegisterRpc("RequestPlayerRespawn", RequestPlayerRespawn, typeof(int));
			networkObject.RegisterRpc("LocalReSpawnService", LocalReSpawnService, typeof(int), typeof(Vector3), typeof(Quaternion));
			networkObject.RegisterRpc("UpdateRoundTime", UpdateRoundTime, typeof(int));
			networkObject.RegisterRpc("ReqSynchGameInfo", ReqSynchGameInfo);
			networkObject.RegisterRpc("ResSynchGameInfo", ResSynchGameInfo, typeof(int), typeof(float));
			networkObject.RegisterRpc("ResRewardPlayerById", ResRewardPlayerById, typeof(int), typeof(int));

			networkObject.onDestroy += DestroyGameObject;

			if (!obj.IsOwner)
			{
				if (!skipAttachIds.ContainsKey(obj.NetworkId)){
					uint newId = obj.NetworkId + 1;
					ProcessOthers(gameObject.transform, ref newId);
				}
				else
					skipAttachIds.Remove(obj.NetworkId);
			}

			if (obj.Metadata != null)
			{
				byte transformFlags = obj.Metadata[0];

				if (transformFlags != 0)
				{
					BMSByte metadataTransform = new BMSByte();
					metadataTransform.Clone(obj.Metadata);
					metadataTransform.MoveStartIndex(1);

					if ((transformFlags & 0x01) != 0 && (transformFlags & 0x02) != 0)
					{
						MainThreadManager.Run(() =>
						{
							transform.position = ObjectMapper.Instance.Map<Vector3>(metadataTransform);
							transform.rotation = ObjectMapper.Instance.Map<Quaternion>(metadataTransform);
						});
					}
					else if ((transformFlags & 0x01) != 0)
					{
						MainThreadManager.Run(() => { transform.position = ObjectMapper.Instance.Map<Vector3>(metadataTransform); });
					}
					else if ((transformFlags & 0x02) != 0)
					{
						MainThreadManager.Run(() => { transform.rotation = ObjectMapper.Instance.Map<Quaternion>(metadataTransform); });
					}
				}
			}

			MainThreadManager.Run(() =>
			{
				NetworkStart();
				networkObject.Networker.FlushCreateActions(networkObject);
			});
		}

		protected override void CompleteRegistration()
		{
			base.CompleteRegistration();
			networkObject.ReleaseCreateBuffer();
		}

		public override void Initialize(NetWorker networker, byte[] metadata = null)
		{
			Initialize(new GameManagerStuffTestNetworkObject(networker, createCode: TempAttachCode, metadata: metadata));
		}

		private void DestroyGameObject(NetWorker sender)
		{
			MainThreadManager.Run(() => { try { Destroy(gameObject); } catch { } });
			networkObject.onDestroy -= DestroyGameObject;
		}

		public override NetworkObject CreateNetworkObject(NetWorker networker, int createCode, byte[] metadata = null)
		{
			return new GameManagerStuffTestNetworkObject(networker, this, createCode, metadata);
		}

		protected override void InitializedTransform()
		{
			networkObject.SnapInterpolations();
		}

		/// <summary>
		/// Arguments:
		/// int requestedPlayerID
		/// </summary>
		public abstract void RequestPlayerRespawn(RpcArgs args);
		/// <summary>
		/// Arguments:
		/// int playerID
		/// Vector3 spawnPointPos
		/// Quaternion spawnPointRot
		/// </summary>
		public abstract void LocalReSpawnService(RpcArgs args);
		/// <summary>
		/// Arguments:
		/// int roundSeconds
		/// </summary>
		public abstract void UpdateRoundTime(RpcArgs args);
		/// <summary>
		/// Arguments:
		/// </summary>
		public abstract void ReqSynchGameInfo(RpcArgs args);
		/// <summary>
		/// Arguments:
		/// </summary>
		public abstract void ResSynchGameInfo(RpcArgs args);
		/// <summary>
		/// Arguments:
		/// </summary>
		public abstract void ResRewardPlayerById(RpcArgs args);

		// DO NOT TOUCH, THIS GETS GENERATED PLEASE EXTEND THIS CLASS IF YOU WISH TO HAVE CUSTOM CODE ADDITIONS
	}
}