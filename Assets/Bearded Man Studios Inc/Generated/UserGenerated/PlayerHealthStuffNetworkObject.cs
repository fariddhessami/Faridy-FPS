using BeardedManStudios.Forge.Networking.Frame;
using BeardedManStudios.Forge.Networking.Unity;
using System;
using UnityEngine;

namespace BeardedManStudios.Forge.Networking.Generated
{
	[GeneratedInterpol("{\"inter\":[0]")]
	public partial class PlayerHealthStuffNetworkObject : NetworkObject
	{
		public const int IDENTITY = 14;

		private byte[] _dirtyFields = new byte[1];

		#pragma warning disable 0067
		public event FieldChangedEvent fieldAltered;
		#pragma warning restore 0067
		[ForgeGeneratedField]
		private int _playerHp;
		public event FieldEvent<int> playerHpChanged;
		public Interpolated<int> playerHpInterpolation = new Interpolated<int>() { LerpT = 0f, Enabled = false };
		public int playerHp
		{
			get { return _playerHp; }
			set
			{
				// Don't do anything if the value is the same
				if (_playerHp == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[0] |= 0x1;
				_playerHp = value;
				hasDirtyFields = true;
			}
		}

		public void SetplayerHpDirty()
		{
			_dirtyFields[0] |= 0x1;
			hasDirtyFields = true;
		}

		private void RunChange_playerHp(ulong timestep)
		{
			if (playerHpChanged != null) playerHpChanged(_playerHp, timestep);
			if (fieldAltered != null) fieldAltered("playerHp", _playerHp, timestep);
		}

		protected override void OwnershipChanged()
		{
			base.OwnershipChanged();
			SnapInterpolations();
		}
		
		public void SnapInterpolations()
		{
			playerHpInterpolation.current = playerHpInterpolation.target;
		}

		public override int UniqueIdentity { get { return IDENTITY; } }

		protected override BMSByte WritePayload(BMSByte data)
		{
			UnityObjectMapper.Instance.MapBytes(data, _playerHp);

			return data;
		}

		protected override void ReadPayload(BMSByte payload, ulong timestep)
		{
			_playerHp = UnityObjectMapper.Instance.Map<int>(payload);
			playerHpInterpolation.current = _playerHp;
			playerHpInterpolation.target = _playerHp;
			RunChange_playerHp(timestep);
		}

		protected override BMSByte SerializeDirtyFields()
		{
			dirtyFieldsData.Clear();
			dirtyFieldsData.Append(_dirtyFields);

			if ((0x1 & _dirtyFields[0]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _playerHp);

			// Reset all the dirty fields
			for (int i = 0; i < _dirtyFields.Length; i++)
				_dirtyFields[i] = 0;

			return dirtyFieldsData;
		}

		protected override void ReadDirtyFields(BMSByte data, ulong timestep)
		{
			if (readDirtyFlags == null)
				Initialize();

			Buffer.BlockCopy(data.byteArr, data.StartIndex(), readDirtyFlags, 0, readDirtyFlags.Length);
			data.MoveStartIndex(readDirtyFlags.Length);

			if ((0x1 & readDirtyFlags[0]) != 0)
			{
				if (playerHpInterpolation.Enabled)
				{
					playerHpInterpolation.target = UnityObjectMapper.Instance.Map<int>(data);
					playerHpInterpolation.Timestep = timestep;
				}
				else
				{
					_playerHp = UnityObjectMapper.Instance.Map<int>(data);
					RunChange_playerHp(timestep);
				}
			}
		}

		public override void InterpolateUpdate()
		{
			if (IsOwner)
				return;

			if (playerHpInterpolation.Enabled && !playerHpInterpolation.current.UnityNear(playerHpInterpolation.target, 0.0015f))
			{
				_playerHp = (int)playerHpInterpolation.Interpolate();
				//RunChange_playerHp(playerHpInterpolation.Timestep);
			}
		}

		private void Initialize()
		{
			if (readDirtyFlags == null)
				readDirtyFlags = new byte[1];

		}

		public PlayerHealthStuffNetworkObject() : base() { Initialize(); }
		public PlayerHealthStuffNetworkObject(NetWorker networker, INetworkBehavior networkBehavior = null, int createCode = 0, byte[] metadata = null) : base(networker, networkBehavior, createCode, metadata) { Initialize(); }
		public PlayerHealthStuffNetworkObject(NetWorker networker, uint serverId, FrameStream frame) : base(networker, serverId, frame) { Initialize(); }

		// DO NOT TOUCH, THIS GETS GENERATED PLEASE EXTEND THIS CLASS IF YOU WISH TO HAVE CUSTOM CODE ADDITIONS
	}
}
