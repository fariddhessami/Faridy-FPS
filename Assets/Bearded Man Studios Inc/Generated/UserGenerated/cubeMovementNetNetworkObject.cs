using BeardedManStudios.Forge.Networking.Frame;
using BeardedManStudios.Forge.Networking.Unity;
using System;
using UnityEngine;

namespace BeardedManStudios.Forge.Networking.Generated
{
	[GeneratedInterpol("{\"inter\":[0.15]")]
	public partial class cubeMovementNetNetworkObject : NetworkObject
	{
		public const int IDENTITY = 5;

		private byte[] _dirtyFields = new byte[1];

		#pragma warning disable 0067
		public event FieldChangedEvent fieldAltered;
		#pragma warning restore 0067
		[ForgeGeneratedField]
		private Vector3 _postiton;
		public event FieldEvent<Vector3> postitonChanged;
		public InterpolateVector3 postitonInterpolation = new InterpolateVector3() { LerpT = 0.15f, Enabled = true };
		public Vector3 postiton
		{
			get { return _postiton; }
			set
			{
				// Don't do anything if the value is the same
				if (_postiton == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[0] |= 0x1;
				_postiton = value;
				hasDirtyFields = true;
			}
		}

		public void SetpostitonDirty()
		{
			_dirtyFields[0] |= 0x1;
			hasDirtyFields = true;
		}

		private void RunChange_postiton(ulong timestep)
		{
			if (postitonChanged != null) postitonChanged(_postiton, timestep);
			if (fieldAltered != null) fieldAltered("postiton", _postiton, timestep);
		}

		protected override void OwnershipChanged()
		{
			base.OwnershipChanged();
			SnapInterpolations();
		}
		
		public void SnapInterpolations()
		{
			postitonInterpolation.current = postitonInterpolation.target;
		}

		public override int UniqueIdentity { get { return IDENTITY; } }

		protected override BMSByte WritePayload(BMSByte data)
		{
			UnityObjectMapper.Instance.MapBytes(data, _postiton);

			return data;
		}

		protected override void ReadPayload(BMSByte payload, ulong timestep)
		{
			_postiton = UnityObjectMapper.Instance.Map<Vector3>(payload);
			postitonInterpolation.current = _postiton;
			postitonInterpolation.target = _postiton;
			RunChange_postiton(timestep);
		}

		protected override BMSByte SerializeDirtyFields()
		{
			dirtyFieldsData.Clear();
			dirtyFieldsData.Append(_dirtyFields);

			if ((0x1 & _dirtyFields[0]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _postiton);

			// Reset all the dirty fields
			for (int i = 0; i < _dirtyFields.Length; i++)
				_dirtyFields[i] = 0;

			return dirtyFieldsData;
		}

		protected override void ReadDirtyFields(BMSByte data, ulong timestep)
		{
			if (readDirtyFlags == null)
				Initialize();

			Buffer.BlockCopy(data.byteArr, data.StartIndex(), readDirtyFlags, 0, readDirtyFlags.Length);
			data.MoveStartIndex(readDirtyFlags.Length);

			if ((0x1 & readDirtyFlags[0]) != 0)
			{
				if (postitonInterpolation.Enabled)
				{
					postitonInterpolation.target = UnityObjectMapper.Instance.Map<Vector3>(data);
					postitonInterpolation.Timestep = timestep;
				}
				else
				{
					_postiton = UnityObjectMapper.Instance.Map<Vector3>(data);
					RunChange_postiton(timestep);
				}
			}
		}

		public override void InterpolateUpdate()
		{
			if (IsOwner)
				return;

			if (postitonInterpolation.Enabled && !postitonInterpolation.current.UnityNear(postitonInterpolation.target, 0.0015f))
			{
				_postiton = (Vector3)postitonInterpolation.Interpolate();
				//RunChange_postiton(postitonInterpolation.Timestep);
			}
		}

		private void Initialize()
		{
			if (readDirtyFlags == null)
				readDirtyFlags = new byte[1];

		}

		public cubeMovementNetNetworkObject() : base() { Initialize(); }
		public cubeMovementNetNetworkObject(NetWorker networker, INetworkBehavior networkBehavior = null, int createCode = 0, byte[] metadata = null) : base(networker, networkBehavior, createCode, metadata) { Initialize(); }
		public cubeMovementNetNetworkObject(NetWorker networker, uint serverId, FrameStream frame) : base(networker, serverId, frame) { Initialize(); }

		// DO NOT TOUCH, THIS GETS GENERATED PLEASE EXTEND THIS CLASS IF YOU WISH TO HAVE CUSTOM CODE ADDITIONS
	}
}
