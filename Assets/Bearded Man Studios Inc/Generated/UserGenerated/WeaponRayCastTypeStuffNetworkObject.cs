using BeardedManStudios.Forge.Networking.Frame;
using BeardedManStudios.Forge.Networking.Unity;
using System;
using UnityEngine;

namespace BeardedManStudios.Forge.Networking.Generated
{
	[GeneratedInterpol("{\"inter\":[0]")]
	public partial class WeaponRayCastTypeStuffNetworkObject : NetworkObject
	{
		public const int IDENTITY = 20;

		private byte[] _dirtyFields = new byte[1];

		#pragma warning disable 0067
		public event FieldChangedEvent fieldAltered;
		#pragma warning restore 0067
		[ForgeGeneratedField]
		private int _alo;
		public event FieldEvent<int> aloChanged;
		public Interpolated<int> aloInterpolation = new Interpolated<int>() { LerpT = 0f, Enabled = false };
		public int alo
		{
			get { return _alo; }
			set
			{
				// Don't do anything if the value is the same
				if (_alo == value)
					return;

				// Mark the field as dirty for the network to transmit
				_dirtyFields[0] |= 0x1;
				_alo = value;
				hasDirtyFields = true;
			}
		}

		public void SetaloDirty()
		{
			_dirtyFields[0] |= 0x1;
			hasDirtyFields = true;
		}

		private void RunChange_alo(ulong timestep)
		{
			if (aloChanged != null) aloChanged(_alo, timestep);
			if (fieldAltered != null) fieldAltered("alo", _alo, timestep);
		}

		protected override void OwnershipChanged()
		{
			base.OwnershipChanged();
			SnapInterpolations();
		}
		
		public void SnapInterpolations()
		{
			aloInterpolation.current = aloInterpolation.target;
		}

		public override int UniqueIdentity { get { return IDENTITY; } }

		protected override BMSByte WritePayload(BMSByte data)
		{
			UnityObjectMapper.Instance.MapBytes(data, _alo);

			return data;
		}

		protected override void ReadPayload(BMSByte payload, ulong timestep)
		{
			_alo = UnityObjectMapper.Instance.Map<int>(payload);
			aloInterpolation.current = _alo;
			aloInterpolation.target = _alo;
			RunChange_alo(timestep);
		}

		protected override BMSByte SerializeDirtyFields()
		{
			dirtyFieldsData.Clear();
			dirtyFieldsData.Append(_dirtyFields);

			if ((0x1 & _dirtyFields[0]) != 0)
				UnityObjectMapper.Instance.MapBytes(dirtyFieldsData, _alo);

			// Reset all the dirty fields
			for (int i = 0; i < _dirtyFields.Length; i++)
				_dirtyFields[i] = 0;

			return dirtyFieldsData;
		}

		protected override void ReadDirtyFields(BMSByte data, ulong timestep)
		{
			if (readDirtyFlags == null)
				Initialize();

			Buffer.BlockCopy(data.byteArr, data.StartIndex(), readDirtyFlags, 0, readDirtyFlags.Length);
			data.MoveStartIndex(readDirtyFlags.Length);

			if ((0x1 & readDirtyFlags[0]) != 0)
			{
				if (aloInterpolation.Enabled)
				{
					aloInterpolation.target = UnityObjectMapper.Instance.Map<int>(data);
					aloInterpolation.Timestep = timestep;
				}
				else
				{
					_alo = UnityObjectMapper.Instance.Map<int>(data);
					RunChange_alo(timestep);
				}
			}
		}

		public override void InterpolateUpdate()
		{
			if (IsOwner)
				return;

			if (aloInterpolation.Enabled && !aloInterpolation.current.UnityNear(aloInterpolation.target, 0.0015f))
			{
				_alo = (int)aloInterpolation.Interpolate();
				//RunChange_alo(aloInterpolation.Timestep);
			}
		}

		private void Initialize()
		{
			if (readDirtyFlags == null)
				readDirtyFlags = new byte[1];

		}

		public WeaponRayCastTypeStuffNetworkObject() : base() { Initialize(); }
		public WeaponRayCastTypeStuffNetworkObject(NetWorker networker, INetworkBehavior networkBehavior = null, int createCode = 0, byte[] metadata = null) : base(networker, networkBehavior, createCode, metadata) { Initialize(); }
		public WeaponRayCastTypeStuffNetworkObject(NetWorker networker, uint serverId, FrameStream frame) : base(networker, serverId, frame) { Initialize(); }

		// DO NOT TOUCH, THIS GETS GENERATED PLEASE EXTEND THIS CLASS IF YOU WISH TO HAVE CUSTOM CODE ADDITIONS
	}
}
