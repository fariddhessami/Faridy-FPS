using BeardedManStudios.Forge.Networking.Frame;
using System;
using MainThreadManager = BeardedManStudios.Forge.Networking.Unity.MainThreadManager;

namespace BeardedManStudios.Forge.Networking.Generated
{
	public partial class NetworkObjectFactory : NetworkObjectFactoryBase
	{
		public override void NetworkCreateObject(NetWorker networker, int identity, uint id, FrameStream frame, Action<NetworkObject> callback)
		{
			if (networker.IsServer)
			{
				if (frame.Sender != null && frame.Sender != networker.Me)
				{
					if (!ValidateCreateRequest(networker, identity, id, frame))
						return;
				}
			}
			
			bool availableCallback = false;
			NetworkObject obj = null;
			MainThreadManager.Run(() =>
			{
				switch (identity)
				{
					case AClassStuffNetworkObject.IDENTITY:
						availableCallback = true;
						obj = new AClassStuffNetworkObject(networker, id, frame);
						break;
					case ChatManagerNetworkObject.IDENTITY:
						availableCallback = true;
						obj = new ChatManagerNetworkObject(networker, id, frame);
						break;
					case ChunkStuffNetworkObject.IDENTITY:
						availableCallback = true;
						obj = new ChunkStuffNetworkObject(networker, id, frame);
						break;
					case CubeForgeGameNetworkObject.IDENTITY:
						availableCallback = true;
						obj = new CubeForgeGameNetworkObject(networker, id, frame);
						break;
					case cubeMovementNetNetworkObject.IDENTITY:
						availableCallback = true;
						obj = new cubeMovementNetNetworkObject(networker, id, frame);
						break;
					case DestroyableBlockStuffNetworkObject.IDENTITY:
						availableCallback = true;
						obj = new DestroyableBlockStuffNetworkObject(networker, id, frame);
						break;
					case ExampleProximityPlayerNetworkObject.IDENTITY:
						availableCallback = true;
						obj = new ExampleProximityPlayerNetworkObject(networker, id, frame);
						break;
					case GameLogicTrainingStuffNetworkObject.IDENTITY:
						availableCallback = true;
						obj = new GameLogicTrainingStuffNetworkObject(networker, id, frame);
						break;
					case GameManagerStuffNetworkObject.IDENTITY:
						availableCallback = true;
						obj = new GameManagerStuffNetworkObject(networker, id, frame);
						break;
					case GameManagerStuffTestNetworkObject.IDENTITY:
						availableCallback = true;
						obj = new GameManagerStuffTestNetworkObject(networker, id, frame);
						break;
					case GuyStuffNetworkObject.IDENTITY:
						availableCallback = true;
						obj = new GuyStuffNetworkObject(networker, id, frame);
						break;
					case NetworkCameraNetworkObject.IDENTITY:
						availableCallback = true;
						obj = new NetworkCameraNetworkObject(networker, id, frame);
						break;
					case ObjectHealthStuffNetworkObject.IDENTITY:
						availableCallback = true;
						obj = new ObjectHealthStuffNetworkObject(networker, id, frame);
						break;
					case PlayerHealthStuffNetworkObject.IDENTITY:
						availableCallback = true;
						obj = new PlayerHealthStuffNetworkObject(networker, id, frame);
						break;
					case PlayerLookStuffNetworkObject.IDENTITY:
						availableCallback = true;
						obj = new PlayerLookStuffNetworkObject(networker, id, frame);
						break;
					case PlayerMoveStuffNetworkObject.IDENTITY:
						availableCallback = true;
						obj = new PlayerMoveStuffNetworkObject(networker, id, frame);
						break;
					case PlayerPropertiesStuffNetworkObject.IDENTITY:
						availableCallback = true;
						obj = new PlayerPropertiesStuffNetworkObject(networker, id, frame);
						break;
					case TestNetworkObject.IDENTITY:
						availableCallback = true;
						obj = new TestNetworkObject(networker, id, frame);
						break;
					case WeaponHolderStuffNetworkObject.IDENTITY:
						availableCallback = true;
						obj = new WeaponHolderStuffNetworkObject(networker, id, frame);
						break;
					case WeaponRayCastTypeStuffNetworkObject.IDENTITY:
						availableCallback = true;
						obj = new WeaponRayCastTypeStuffNetworkObject(networker, id, frame);
						break;
				}

				if (!availableCallback)
					base.NetworkCreateObject(networker, identity, id, frame, callback);
				else if (callback != null)
					callback(obj);
			});
		}

		// DO NOT TOUCH, THIS GETS GENERATED PLEASE EXTEND THIS CLASS IF YOU WISH TO HAVE CUSTOM CODE ADDITIONS
	}
}