﻿using System.Collections;
using System.Collections.Generic;
using BeardedManStudios.Forge.Networking;
using BeardedManStudios.Forge.Networking.Generated;
using UnityEngine;

public class WeaponHolder : WeaponHolderStuffBehavior
{

    [SerializeField] private bool is3rdPerson;
    
    [SerializeField] internal WeaponHolder _3rdPersonRef;
    
    [SerializeField] internal WeaponRayCastType Weapon_1;

    [SerializeField] private bool display_weaponHolder;
    
    [SerializeField] private PlayerMove _ownerPlayerMove;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    protected override void NetworkStart()
    {
        base.NetworkStart();


        if (is3rdPerson)
        {
            // this means this weapon holder is used as 3rd person,
            // therefore it should not be animated
            Weapon_1.GetComponent<Animator>().enabled = false;
            Weapon_1.IsEffectsShown = true;
        }
    }


    public void Turn_display_on()
    {
        ChangeDisplay(true);
    }
    
    public void Turn_display_off()
    {
        ChangeDisplay(false);
    }

    private void ChangeDisplay(bool isOn)
    {
        display_weaponHolder = isOn;
        
        Weapon_1.IsEffectsShown = isOn;
        Weapon_1.GetComponentInChildren<MeshRenderer>().enabled = isOn;
    }
    
    
    public void Make3rdP_WeaponShootEffect(int weaponIndex)
    {
        if (!is3rdPerson)
        {
            _3rdPersonRef.Weapon_1.ShootJustEffect();
        }
    }

    public override void changeWeaponReq(RpcArgs args)
    {
        throw new System.NotImplementedException();
    }

    public override void changeWeaponRes(RpcArgs args)
    {
        throw new System.NotImplementedException();
    }

    public PlayerMove GetOwnerPlayerMove()
    {
        return _ownerPlayerMove;
    }

    public bool isThirdPerson()
    {
        return is3rdPerson;
    }
}
